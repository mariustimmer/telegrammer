��            )   �      �  *   �     �     �     �               :  /   N     ~     �  -   �     �  ~   �     n     �     �     �  !   �  !   �  $     ,   =  $   j     �     �     �     �  	   �  #   �  X  !  /   z     �     �  !   �       &   !     H  A   `     �  !   �  -   �     	  �   $	     �	  !   �	  #   �	     
  (   /
  (   X
  .   �
  /   �
  ,   �
  &        4     H  $   ]  	   �  %   �                   
                                                                                        	                               %s, you know this is not working right now *Fefe*: %s [Link](%s) Could not download RSS Could not download file! Could not find sql script! Could not fork to background Could not parse XML Could not read configuration file for no reason Could not speak the text Could not write text file Finished                                    
 Forecast for %s Hello %s! Unfortunately I am not allowed to talk to you. First you have to contact my admin who may put you on the white list. I don't know the city of "%s". I hope I could help you. I never heard about "%s". No API key for owm! No forecast available for %s [%d] No weather data found for %s (%d) Sorry but I don't know that command. The configuration file '%s' is not readable! There is no configuration file '%s'! This are the 10 newest messages Unexpected response Updating %d cities Weather summary for %s at %s Y-m-d H:i You've got %d messages in your mbox Project-Id-Version: telegrammer 1.0
Report-Msgid-Bugs-To: Marius Timmer <admin@mariustimmer.de>
PO-Revision-Date: 2020-09-18 16:42+0200
Last-Translator: Marius Timmer <admin@mariustimmer.de>
Language-Team: German <admin@mariustimmer.de>
Language: German
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
 %s, du weißt, dass das noch nicht funktioniert *Fefe*: %s [Link](%s) Konnte RSS nicht herunterladen Konnte Datei nicht herunterladen! Konnte SQL-Skript nicht finden! Konnte nicht in den Hintergrund forken Konnte XML nicht parsen Konnte aus irgendeinem Grund die Konfigurations-Datei nicht lesen Konnte den Text nicht sprechen Konnte Text-Datei nicht schreiben Abgeschlossen                               
 Vorhersage für %s Hallo %s! Leider darf ich nicht mit dir reden. Erst mal solltest du meinen Admin fragen, der dich in die positiv-Liste packen könnte. Ich kenne die Stadt "%s" nicht. Ich hoffe, ich konnte dir helfen. Ich habe noch nie von "%s" gehört. Kein API-Schlüssel für OWM! Keine Vorhersage für %s [%d] verfügbar Keine Wetter-Daten für %s (%s) gefunden Entschuldige, aber den Befehl kenne ich nicht. Die Konfigurations-Datei '%s' ist nicht lesbar! Es gibt die Konfigurations-Datei '%s' nicht! Dies sind die neusten zehn Nachrichten Unerwartete Antwort Aktualisiere %d Orte Wetter-Zusammenfassung für %s um %s d.m.Y H:i Du hast %d Nachrichten in deiner MBox 