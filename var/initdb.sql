CREATE TABLE coordinates (
    id SERIAL PRIMARY KEY,
    longitude REAL NOT NULL,
    latitude REAL NOT NULL,
    altitude REAL,
    added TIMESTAMP DEFAULT CURRENT_TIMESTAMP
);
CREATE TABLE cities (
    id INTEGER PRIMARY KEY,
    name VARCHAR(256) NOT NULL,
    country VARCHAR(4) NOT NULL,
    coordinates INTEGER,
    population INTEGER DEFAULT 0,
    timezone INTEGER DEFAULT 0,
    added TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
    CONSTRAINT fk_cities_coordinates FOREIGN KEY (coordinates) REFERENCES coordinates(id)
);
CREATE TABLE weather_forecast (
    id SERIAL PRIMARY KEY,
    city INTEGER NOT NULL,
    dt TIMESTAMP NOT NULL,
    temp FLOAT NOT NULL,
    temp_min FLOAT NOT NULL,
    temp_max FLOAT NOT NULL,
    pressure FLOAT NOT NULL,
    humidity INTEGER NOT NULL,
    description VARCHAR NOT NULL,
    wind FLOAT NOT NULL,
    added TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
    CONSTRAINT fk_weather_forecast_city FOREIGN KEY (city) REFERENCES cities(id)
);
CREATE TABLE users (
    id SERIAL,
    first_name VARCHAR(64) NOT NULL,
    last_name VARCHAR(64) DEFAULT NULL,
    username VARCHAR(128) DEFAULT '',
    language_code VARCHAR(5) DEFAULT NULL,
    added TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
    CONSTRAINT pk_users PRIMARY KEY (id)
);
CREATE TABLE roles (
    id SERIAL,
    priority INTEGER DEFAULT 0,
    name VARCHAR(32) NOT NULL,
    added TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
    CONSTRAINT pk_roles PRIMARY KEY (id),
    CONSTRAINT uc_roles_name UNIQUE (name)
);
CREATE TABLE user_roles (
    userid INTEGER NOT NULL,
    roleid INTEGER NOT NULL,
    added TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
    CONSTRAINT pk_user_roles PRIMARY KEY (userid, roleid),
    CONSTRAINT fk_user_roles_users FOREIGN KEY (userid) REFERENCES users(id),
    CONSTRAINT fk_user_roles_roles FOREIGN KEY (roleid) REFERENCES roles(id)
);
CREATE TABLE weather_observations (
    id SERIAL,
    userid INTEGER NOT NULL,
    cityid INTEGER NOT NULL,
    added TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
    CONSTRAINT pk_weather_observations PRIMARY KEY (id),
    CONSTRAINT fk_weather_observations_users FOREIGN KEY (userid) REFERENCES users(id),
    CONSTRAINT fk_weather_observations_cities FOREIGN KEY (cityid) REFERENCES cities(id)
);
CREATE TABLE chats (
    id INTEGER NOT NULL,
    type VARCHAR(16) DEFAULT 'private',
    title VARCHAR(256) DEFAULT NULL,
    all_members_are_admins INTEGER DEFAULT 0,
    description VARCHAR(512) DEFAULT NULL,
    added TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
    CONSTRAINT pk_chats PRIMARY KEY (id)
);
CREATE TABLE messages (
    id INTEGER NOT NULL,
    from_id INTEGER,
    date TIMESTAMP NOT NULL,
    chat_id INTEGER,
    reply_to_message_id INTEGER,
    edit_date TIMESTAMP DEFAULT NULL,
    text TEXT DEFAULT NULL,
    voice TEXT DEFAULT NULL,
    added TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
    CONSTRAINT pk_messages PRIMARY KEY (id),
    CONSTRAINT fk_messages_users_from_id FOREIGN KEY (from_id) REFERENCES users(id),
    CONSTRAINT fk_messages_chats_chat_id FOREIGN KEY (chat_id) REFERENCES chats(id),
    CONSTRAINT fk_messages_messages_reply_to_message_id FOREIGN KEY (reply_to_message_id) REFERENCES messages(id)
);
CREATE TABLE updates (
    id INTEGER NOT NULL,
    message_id INTEGER,
    edited_message_id INTEGER,
    added TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
    CONSTRAINT pk_updates PRIMARY KEY (id),
    CONSTRAINT fk_updates_messages_message_id FOREIGN KEY (message_id) REFERENCES messages(id),
    CONSTRAINT fk_updates_messages_edited_message_id FOREIGN KEY (edited_message_id) REFERENCES messages(id)
);
CREATE TABLE words (
    id SERIAL,
    text TEXT NOT NULL,
    type INTEGER NOT NULL,
    language VARCHAR(3) NOT NULL DEFAULT 'en',
    added TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
    CONSTRAINT pk_words PRIMARY KEY (id),
    CONSTRAINT uc_words_text UNIQUE (text)
);
CREATE TABLE nouns (
    id SERIAL,
    word_id INTEGER NOT NULL,
    genus INTEGER DEFAULT 1,
    own_name INTEGER DEFAULT 0,
    added TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
    CONSTRAINT pk_nouns PRIMARY KEY (id),
    CONSTRAINT fk_nouns_words FOREIGN KEY (word_id) REFERENCES words(id)
);


CREATE VIEW read_userroles AS
    SELECT users.id AS userid, users.username, roles.id AS roleid, roles.name FROM users LEFT JOIN user_roles ON (users.id = user_roles.userid) LEFT JOIN roles ON (user_roles.roleid = roles.id);

-- Lets fill some data
INSERT INTO users (id, first_name, last_name, username, language_code) VALUES
    (161707362, 'Marius', 'Timmer', 'mariustimmer', 'de');

INSERT INTO roles (id, priority, name) VALUES
    (1, 100, 'Administrator'),
    (2, 30, 'User'),
    (3, 20, 'Guest'),
    (4, 10, 'Stranger');

INSERT INTO user_roles (userid, roleid) VALUES
     (161707362, 1);
