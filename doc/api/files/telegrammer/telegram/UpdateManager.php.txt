<?php

namespace telegrammer\telegram;

use \Exception AS Exception;

class UpdateManager {

    const UPDATES_FILE = ROOT_DIRECTORY . DIRECTORY_SEPARATOR .'updates.json';
    const KEY_CREATED  = 'created';
    const KEY_UPDATES  = 'updates';

    private static $instance;

    /**
     *
     * @var int
     */
    private $created;

    /**
     *
     * @var Update[]
     */
    private $updates;

    private function __construct() {
        $this->load();
    }

    public static function getInstance(): UpdateManager {
        if (!self::$instance) {
            self::$instance = new UpdateManager();
        }
        return self::$instance;
    }

    /**
     * 
     * @param int $chat_id
     * @return Message[]
     */
    public function getConversation(int $chat_id) {
        $messages = [];
        foreach ($this->updates AS $update) {
            if ($update->getMessage()->getChat()->getID() === $chat_id) {
                array_push($messages, $update->getMessage());
            }
        }
        return $messages;
    }

    public function contains($update): bool {
        if (is_array($update)) {
            $update = new Update($update);
        }
        foreach ($this->updates AS $current_update) {
            if ($current_update->getID() == $update->getID()) {
                return true;
            }
        }
        return false;
    }

    public function add($update) {
        if (is_array($update)) {
            $update = new Update($update);
        }
        array_push($this->updates, $update);
        $this->store();
    }

    public function store() {
        $json_array = [];
        foreach ($this->updates AS $update) {
            array_push($json_array, $update->toArray());
        }
        $content = [
            self::KEY_CREATED => $this->created,
            self::KEY_UPDATES => $json_array
        ];
        return file_put_contents(
            self::UPDATES_FILE,
            json_encode(
                $content,
                JSON_PRETTY_PRINT
            )
        );
    }

    private function load() {
        if (!file_exists(self::UPDATES_FILE)) {
            $init_content = [
                self::KEY_CREATED => time(),
                self::KEY_UPDATES => []
            ];
            file_put_contents(
                self::UPDATES_FILE,
                json_encode($init_content, JSON_PRETTY_PRINT)
            );
        }
        $content = file_get_contents(self::UPDATES_FILE);
        if (!$content) {
            throw new Exception(gettext("Could not read updates file"));
        }
        $json = json_decode($content, true);
        if (!$json) {
            throw new Exception(gettext("invalid updates file"));
        }
        $this->created = $json[self::KEY_CREATED];
        $this->updates = [];
        foreach ($json[self::KEY_UPDATES] AS $json_update) {
            $this->add($json_update);
        }
    }

}
