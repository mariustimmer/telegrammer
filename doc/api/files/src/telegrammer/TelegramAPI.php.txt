<?php

namespace telegrammer;

use \CURLFile AS CURLFile;
use \Exception AS Exception;
use \telegrammer\Configuration AS Configuration;
use \telegrammer\Espeak AS Espeak;
use \telegrammer\telegram\File AS File;
use \telegrammer\telegram\Update AS Update;

class TelegramAPI {

    const ADMIN_NAME         = 0;
    const ADMIN_CHATID       = 1;
    const URL_TEMPLATE       = 'https://api.telegram.org/bot%s/%s';
    const PARSEMODE_TEXT     = '';
    const PARSEMODE_MARKDOWN = 'Markdown';

    /**
     * Singelton object
     * @var TelegramAPI
     */
    private static $instance;

    /**
     * Sends a message to a chat.
     * @param int $chat_id Chat-ID
     * @param string $message Message
     * @param string $parse_mode Mode of the message (PARSEMODE_TEXT or PARSEMODE_MARKDOWN)
     * @return bool
     */
    public function send(int $chat_id, string $message, string $parse_mode = self::PARSEMODE_TEXT): bool {
        return is_array(
            $this->communicate(
                'sendMessage',
                [
                    'chat_id'    => $chat_id,
                    'text'       => $message,
                    'parse_mode' => $parse_mode
                ]
            )
        );
    }

    public function sendVoice(int $chat_id, string $message, string $caption = null) {
        Espeak::getInstance()->setVoice(Espeak::VOICE_GERMAN_FEMALE);
        $curlfile = new CURLFile(Espeak::getInstance()->textToSpeech($message));
        return is_array(
            $this->communicate(
                'sendVoice',
                [
                    'chat_id'    => $chat_id,
                    'voice'      => $curlfile,
                    'caption'    => $caption
                ]
            )
        );
    }

    public function getFile(string $file_id): File {
        $response = $this->communicate(
            'getFile',
            [
                'file_id' => $file_id
            ]
        );
        if (empty($response['result'])) {
            return null;
        }
        return new File($response['result']);
    }

    public function downloadFile(File $file) {
        $url = sprintf(
            'https://api.telegram.org/file/bot%s/%s',
            Configuration::getInstance()->get('Telegram', 'APIKEY'),
            $file->getFilePath()
        );
        $response = file_get_contents($url);
        if ($response === false) {
            throw new Exception(gettext("Could not download file!"));
        }
        var_dump(dirname($file->getFilePath()));
        file_put_contents(ROOT_DIRECTORY . DIRECTORY_SEPARATOR .basename($file->getFilePath()), $response);
    }

    /**
     * 
     * @return Update[]
     */
    public function getUpdates(): array {
        $response = $this->communicate(
            'getUpdates'
        );
        $updates = [];
        foreach ($response['result'] AS $json_update) {
            array_push($updates, new Update($json_update));
        }
        return $updates;
    }

    public function getMe() {
        return $this->communicate('getMe');
    }

    private function communicate(string $method, array $parameter = []): array {
        $url = sprintf(
            self::URL_TEMPLATE,
            Configuration::getInstance()->get('Telegram', 'APIKEY'),
            $method
        );
        $curl_handler = curl_init();
        curl_setopt_array(
            $curl_handler,
            [
                CURLOPT_URL            => $url,
                CURLOPT_RETURNTRANSFER => 1,
                CURLOPT_POST           => true,
                CURLOPT_POSTFIELDS     => $parameter,
                CURLOPT_HTTPHEADER     => ["Content-Type: multipart/form-data"]
            ]
        );
        $response = curl_exec($curl_handler);
        if ($response === false) {
            throw new Exception(curl_error($curl_handler));
        }
        $json = json_decode($response, true);
        if ((!is_array($json)) ||
            (empty($json['ok'])) ||
            (!$json['ok'])) {
            throw new Exception($json['description'], $json['error_code']);
        }
        return $json;
    }

    /**
     * Reads out information about the administrator.
     * @param int $info ADMIN_CHATID or ADMIN_NAME
     * @return string
     */
    public function getAdmin(int $info = self::ADMIN_CHATID) {
        $admin = explode(':', Configuration::getInstance()->get('Telegram', 'ADMINISTRATOR'));
        return $admin[$info];
    }

    public static function getInstance(): TelegramAPI {
        if (!self::$instance) {
            self::$instance = new TelegramAPI();
        }
        return self::$instance;
    }

}

