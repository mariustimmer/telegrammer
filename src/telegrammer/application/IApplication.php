<?php

namespace telegrammer\application;

interface IApplication {

    /**
     * Returns the current process id.
     * @return int PID
     */
    public function getPID(): int;

    /**
     * Returns the name of the application.
     * @return string Name of the application
     */
    public static function getName(): string;
}
