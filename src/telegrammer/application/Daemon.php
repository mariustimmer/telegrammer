<?php

namespace telegrammer\application;

use \telegrammer\Runtime AS Runtime;

class Daemon extends AbstractApplication {

    const DEFAULT_INTERVALL = 30;

    public function __construct() {
        if (!Runtime::startNewProcess()) {
            // Lets stop the parent so we really have a child
            exit(0);
        }
        pcntl_signal(SIGUSR1, [$this, 'onSignal']);
        Server::buildInstance();
        $this->run();
    }

    public function onSignal(int $signo) {
        switch ($signo) {
            case SIGUSR1:
                print 'Mein Kind ist gestorben!'."\n";
                break;
        }
    }

    /**
     * Returns the time between execution intervalls in seconds .
     * @return int
     */
    public function getIntervall(): int {
        return self::DEFAULT_INTERVALL;
    }

    private function run() {
        print 'Betrete Endlosschleife'."\n";
        while (true) {
            
            sleep($this->getIntervall());
        }
    }

}
