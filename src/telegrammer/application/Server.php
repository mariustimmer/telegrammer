<?php

namespace telegrammer\application;

use \telegrammer\Runtime AS Runtime;

class Server extends AbstractApplication {

    /**
     * Creates a new instance of the server within its own process. After
     * running the server, the process will inform it's parent (SIGUSR1)
     * and exit.
     */
    public static function buildInstance() {
        $parent_pid = getmypid();
        if (!Runtime::startNewProcess()) {
            // This is the child which will work on its own now
            posix_setsid();
            $server = new Server();
            $server->run();
            posix_kill($parent_pid, SIGUSR1);
            exit(0);
        }
    }

    private function run() {
        print 'ICH BIN DER SERVER'."\n";
    }

}
