<?php

namespace telegrammer\application;

abstract class AbstractApplication implements IApplication {

    public function getPID(): int {
        return getmypid();
    }

    public static function getName(): string {
        return __CLASS__;
    }

}
