<?php

namespace telegrammer;

use \telegrammer\plugins\MBox AS MBox;
use \telegrammer\database\CityManager AS CityManager;
use \telegrammer\database\WeatherManager AS WeatherManager;
use \telegrammer\plugins\WeatherDrawer AS WeatherDrawer;
use \telegrammer\Telegram\Message AS Message;
use \telegrammer\Telegram\User AS User;
use \telegrammer\TelegramAPI AS TelegramAPI;

class Flynn {

    const COMMAND_MAIL = 'mail';
    const DESCRIPTION_MAIL = 'Informs you about mails or sends you a requested mail';
    const EXAMPLE_MAIL = '/mail [id]';

    const LOCALE_ENGLISH = 'en';
    const LOCALE_GERMAN  = 'de';
    const LOCALE_DEFAULT = self::LOCALE_ENGLISH;
    const TEXT_DOMAIN = 'telegrammer';

    const COMMAND_WEATHER     = 'weather';
    const DESCRIPTION_WEATHER = 'Sends you the weather forecast';
    const EXAMPLE_WEATHER     = '/weather [city]';

    /**
     * Singelton object.
     * @var Flynn
     */
    private static $instance;

    public static function getInstance(): Flynn {
        if (!self::$instance) {
            self::$instance = new Flynn();
        }
        return self::$instance;
    }

    /**
     * Decides which is the prefered voice to use in voice messages for the
     * given user. The users language will be included in this decision.
     * @param User $user
     * @return string
     */
    public static function getPreferedVoice(User $user) {
        $lang_code = strtolower(trim($user->getLanguageCode()));
        if (empty($lang_code)) {
            return Espeak::VOICE_DEFAULT;
        } else if (substr($lang_code, 0, 2) == 'de') {
            if ($user->getUsername() === TelegramAPI::getInstance()->getAdmin(TelegramAPI::ADMIN_NAME)) {
                /**
                 * Our german administrators will get a "female" voice.
                 */
                return Espeak::VOICE_GERMAN_FEMALE;
            }
            return Espeak::VOICE_GERMAN_MALE;
        }
        return Espeak::VOICE_DEFAULT;
    }

    /**
     * Sets the locale so the gettext methods will return the
     * contents in the correct language.
     * @param string $language_code Language code
     */
    private static function initLocale(string $language_code) {
        $locale = self::LOCALE_DEFAULT;
        switch (substr(trim(strtolower($language_code)), 0, 2)) {
            case self::LOCALE_GERMAN:
                $locale = self::LOCALE_GERMAN;
                break;
            case self::LOCALE_ENGLISH:
            default:
                $locale = self::LOCALE_DEFAULT;
        }
        setlocale(LC_MESSAGES, $locale);
        putenv('LC_ALL=' . $locale);
        bindtextdomain(self::TEXT_DOMAIN, ROOT_DIRECTORY .'/var/locale');
        textdomain(self::TEXT_DOMAIN);
    }

    /**
     * Processes a incomming message. If it is a command it will parsed and
     * just executed. If the message is not a command we assume that it is
     * a normal human readable sentences which will tried to be parsed.
     * @param Message $message
     */
    public function processMessage(Message $message) {
        $from_admin = ($message->getChat()->getID() == TelegramAPI::getInstance()->getAdmin());
        self::initLocale($message->getFrom()->getLanguageCode());
        if (!$from_admin) {
            $message->getFrom()->getLanguageCode();
            TelegramAPI::getInstance()->sendVoice(
                $message->getChat()->getID(),
                sprintf(
                    gettext("Hello %s! Unfortunately I am not allowed to talk to you. First you have to contact my admin who may put you on the white list."),
                    $message->getFrom()->getFirstName()
                ),
                self::getPreferedVoice($message->getFrom())
            );
            return;
        }
        if (!empty($message->getText())) {
            if (substr(trim($message->getText()), 0, 1) === '/') {
                /**
                 * This is a simple command we can parse
                 */
                $this->processCommand($message);
            } else {
                TelegramAPI::getInstance()->send(
                    $message->getFrom()->getID(),
                    sprintf(
                        gettext("%s, you know this is not working right now"),
                        $message->getFrom()->getFirstName()
                    ),
                    TelegramAPI::PARSEMODE_MARKDOWN
                );
            }
        } else if (!empty($message->getVoice())) {
            $file = TelegramAPI::getInstance()->getFile($message->getVoice()->getFileID());
            TelegramAPI::getInstance()->downloadFile($file);
        }
    }

    /**
     * Processes a incomming command.
     * @param Message $message
     */
    private function processCommand(Message $message) {
        $response = '';
        $arguments = explode(' ', substr(trim($message->getText()), 1));
        $command = strtolower(array_shift($arguments));
        switch ($command) {
            case self::COMMAND_MAIL:
                if ((count($arguments) === 1) &&
                    (strtolower(trim($arguments[0])) === 'help')) {
                    $response = sprintf(
                        "%s:\n```text\n%s\n```",
                        gettext(self::DESCRIPTION_MAIL),
                        self::EXAMPLE_MAIL
                    );
                } else {
                    $response = $this->commandMail(count($arguments) === 1 ? $arguments[0] : '');
                }
                break;
            case self::COMMAND_WEATHER:
                $city = (count($arguments) === 0) ? '' : $arguments[0];
                $response = $this->commandWeather($message, $city);
                break;
            default:
                $response = gettext("Sorry but I don't know that command.");
        }
        TelegramAPI::getInstance()->send(
            $message->getFrom()->getID(),
            $response,
            TelegramAPI::PARSEMODE_MARKDOWN
        );
    }

    /**
     * Finds the requested city or uses the observed cities of the admin,
     * builds weather forecast graphs and sends them to the user before
     * quiting with a final response.
     * @param Message $message Message to react to (required to get the users
     *                         name and the chat ID)
     * @param string $city     ID of the city to look for
     * @return string          Message to send
     */
    private function commandWeather(Message $message, string $city = ''): string {
        if (empty(trim($city))) {
            $preferences = WeatherManager::getInstance()->getObservedCitiesOfUser($message->getFrom());
            $city = [];
            foreach ($preferences AS $pref) {
                $cid = intval($pref['cityid']);
                if ($cid !== 0) {
                    array_push(
                        $city,
                        CityManager::getInstance()->getCity($pref['cityid'])
                    );
                }
            }
        } else {
            $id = CityManager::getInstance()->getCityID($city);
            if ($id !== null) {
                $found = CityManager::getInstance()->getCity(CityManager::getInstance()->getCityID($city));
                if ($found != null) {
                    $city = [$found];
                } else {
                    return sprintf(
                        gettext("I don't know the city of \"%s\"."),
                        $city
                    );
                }
            } else {
                return sprintf(
                    gettext("I never heard about \"%s\"."),
                    $city
                );
            }
        }
        foreach ($city AS $current_city) {
            $filename = WeatherDrawer::getInstance()->generateForecastSummaryForCity($current_city);
            $caption = sprintf(
                gettext("Forecast for %s"),
                $current_city->getName()
            );
            TelegramAPI::getInstance()->sendPhoto(
                $message->getFrom()->getID(),
                $filename,
                $caption
            );
        }
        return gettext("I hope I could help you.");
    }

    /**
     * Executes the mail command.
     * @param string $id (optional) Message-ID
     * @return string Message to send back
     */
    private function commandMail(string $id = ''): string {
        $message = '';
        $mail = MBox::getInstance()->getMail($id);
        if ($mail) {
            $message = sprintf(
                "Mail from *%s* (_%s_):\n```text\n%s\n```",
                $mail->getFromName(),
                $mail->getSubject(),
                utf8_encode(quoted_printable_decode(trim($mail->getBody())))
            );
        } else {
            /**
             * No mail was requested so we are just giving a summary
             */
            $mails = MBox::getInstance()->getMails();
            $message = sprintf(
                gettext("You've got %d messages in your mbox"),
                count($mails)
            );
            if (count($mails) > 0) {
                if (count($mails) > 5) {
                    $message .= '. '. gettext("This are the 10 newest messages");
                }
                $message .= ":\n\n";
                $i = 0;
                foreach (array_reverse($mails) AS $mail) {
                    if ($i > 5) {
                        continue;
                    }
                    $message .= sprintf(
                        " - \"%s\" %s\n",
                        $mail->getSubject(),
                        $mail->getFromName()
                    );
                    $i++;
                }
            }
        }
        return $message;
    }

    private function processSentence(string $sentence) {
        
    }

}
