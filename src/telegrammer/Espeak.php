<?php

namespace telegrammer;

use \Exception AS Exception;

class Espeak {

    const DIRECTORY = ROOT_DIRECTORY . DIRECTORY_SEPARATOR .'var'. DIRECTORY_SEPARATOR .'espeak';

    const DEFAULT_BASE_FILENAME = 'text';

    const VOICE_ENGLISH_MALE   = 'english';
    const VOICE_ENGLISH_FEMALE = 'us-mbrola-1';
    const VOICE_GERMAN_MALE    = 'german';
    const VOICE_GERMAN_FEMALE  = 'german-mbrola-5';
    const VOICE_DEFAULT        = self::VOICE_ENGLISH_MALE;

    /**
     * Current voice
     * @var string
     */
    private $voice;

    private function __construct() {
        $this->setVoice(self::VOICE_ENGLISH_MALE);
    }

    /**
     * Static stored instance of this Espeak class.
     * @var Espeak
     */
    private static $instance;

    public static function getInstance(): Espeak {
        if (!self::$instance) {
            self::$instance = new Espeak();
        }
        return self::$instance;
    }

    /**
     * Sets a voice (Select one of the VOICE_* constants)
     * @param string $voice
     */
    public function setVoice(string $voice) {
        $this->voice = $voice;
    }

    /**
     * Ensures that there is an empty directory to write the text and wav file to.
     */
    private static function provideEmptyDirectory() {
        if (!file_exists(self::DIRECTORY)) {
            mkdir(self::DIRECTORY, 0700, true);
        }
        @unlink(self::DIRECTORY . DIRECTORY_SEPARATOR .'*');
    }

    public function textToSpeech(string $text, string $base_filename = self::DEFAULT_BASE_FILENAME): string {
        self::provideEmptyDirectory();
        $textfile = self::DIRECTORY . DIRECTORY_SEPARATOR . $base_filename .'.txt';
        $wavefile = self::DIRECTORY . DIRECTORY_SEPARATOR . $base_filename .'.wav';
        $command = sprintf(
            'espeak -f %s -v %s -w %s',
            escapeshellarg($textfile),
            escapeshellarg($this->voice),
            escapeshellarg($wavefile)
        );
        $return_value = null;
        if (file_put_contents($textfile, $text) === false) {
            throw new Exception(gettext("Could not write text file"));
        }
        if (system($command, $return_value) === false) {
            throw new Exception(gettext("Could not speak the text"));
        }
        return $wavefile;
    }

}
