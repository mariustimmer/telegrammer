<?php

namespace telegrammer\openweathermap;

use \telegrammer\JSONable AS JSONable;

class Forecast extends JSONable {

    const KEY_LIST = 'list';
    const KEY_CITY = 'city';

    /**
     *
     * @var Weather[]
     */
    private $list;

    /**
     *
     * @var City
     */
    private $city;

    function getList(): array {
        return $this->list;
    }

    function getCity(): City {
        return $this->city;
    }

    function setList(array $list) {
        $this->list = $list;
    }

    function setCity(City $city) {
        $this->city = $city;
    }

    
    public function __construct(array $array) {
        $this->list = [];
        foreach ($array[self::KEY_LIST] AS $weather) {
            array_push($this->list, new Weather($weather));
        }
        $this->city = new City($array[self::KEY_CITY]);
    }

    public function toArray(): array {
        $list = [];
        foreach ($this->list AS $weather) {
            array_push($list, $weather->toArray());
        }
        return [
            self::KEY_LIST => $list,
            self::KEY_CITY => $this->getCity()->toArray()
        ];
    }

}
