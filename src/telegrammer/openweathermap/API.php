<?php

namespace telegrammer\openweathermap;

use \Exception AS Exception;
use \telegrammer\Configuration AS Configuration;
use \telegrammer\database\CityManager AS CityManager;
use \telegrammer\database\WeatherManager AS WeatherManager;

class API {

    const UNIT_METRIC   = 'metric';
    const UNIT_IMPERIAL = 'imperial';
    const FILE_CITIES = ROOT_DIRECTORY . DIRECTORY_SEPARATOR .'var'. DIRECTORY_SEPARATOR .'owm_cities.json';
    const URL_CITIES            = 'https://bulk.openweathermap.org/sample/city.list.json.gz';
    const TEMPLATE_URL_FORECAST = 'https://api.openweathermap.org/data/2.5/forecast?q=%s&units=%s&APPID=%s';

    /**
     *
     * @var API
     */
    private static $instance;

    /**
     *
     * @var string
     */
    private $unit;

    private function __construct(string $unit = self::UNIT_METRIC) {
        $this->setUnit($unit);
    }

    public static function getInstance(): API {
        if (!self::$instance) {
            self::$instance = new API();
        }
        return self::$instance;
    }

    public function getUnit(): string {
        return $this->unit;
    }

    public function setUnit(string $unit) {
        $this->unit = $unit;
    }

    /**
     * Downloads from a given URL.
     * @param string $url
     * @param bool $json_expected
     * @return type
     * @throws Exception
     */
    private function download(string $url, bool $json_expected = true) {
        $curl_handler = curl_init();
        curl_setopt_array(
            $curl_handler,
            [
                CURLOPT_URL            => $url,
                CURLOPT_RETURNTRANSFER => 1,
                CURLOPT_USERAGENT      => 'Flynn bot'
            ]
        );
        $response = curl_exec($curl_handler);
        if ($response === false) {
            throw new Exception(curl_error($curl_handler));
        }
        $json = json_decode($response, true);
        if (($json_expected) && (
                (!is_array($json)) ||
                (empty($json['list'])))) {
            throw new Exception(gettext("Unexpected response"));
        }
        return $json;
    }

    /**
     * Provides the newest weather forecast.
     * @param string $city Name of the city
     * @param string $country_code Country code (e.g.: de, us, etc.)
     * @return \telegrammer\openweathermap\Forecast
     * @throws Exception
     */
    public function getForecast(string $city, string $country_code = ''): Forecast {
        if (!empty($country_code)) {
            $city .= ','. $country_code;
        }
        $apikey = Configuration::getInstance()->get('OpenWeatherMap', 'APIKEY');
        if (empty($apikey)) {
            throw new Exception(gettext("No API key for owm!"));
        }
        $url = sprintf(
            self::TEMPLATE_URL_FORECAST,
            $city,
            $this->getUnit(),
            $apikey
        );
        return new Forecast($this->download($url));
    }

    /**
     * Downloads the compressed list of available cities and writes it into FILE_CITIES.
     * @return boolean
     */
    public function updateCities() {
        $response = file_get_contents(self::URL_CITIES);
        $content = gzdecode($response);
        if ($content === false) {
            return false;
        }
        $json = json_decode($content, true);
        if (!is_array($json)) {
            return false;
        }
        $all = count($json);
        printf(
            "> %s\n",
            sprintf(
                gettext("Updating %d cities"),
                $all
            )
        );
        $i = 0;
        foreach ($json AS $item) {
            $city = new City($item);
            printf(
                chr(13) .'% -50s',
                substr(
                    sprintf(
                        ' %03d %% %s (%s)',
                        intval(($i/$all) * 100),
                        $city->getName(),
                        $city->getCountry()
                    ),
                    0,
                    50
                )
            );
            CityManager::getInstance()->addCity($city);
            $i++;
        }
        print chr(13). gettext("Finished                                    \n");
    }

    public function updateForecasts(string $city) {
        $forecast = $this->getForecast($city);
        WeatherManager::getInstance()->addForecast($forecast);
        
    }

}
