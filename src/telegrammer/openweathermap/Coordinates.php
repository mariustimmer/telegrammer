<?php

namespace telegrammer\openweathermap;

use \telegrammer\JSONable AS JSONable;

class Coordinates extends JSONable {

    const KEY_LATITUDE  = 'lat';
    const KEY_LONGITUDE = 'lon';

    private $longitude;
    private $latitude;

    function getLongitude() {
        return $this->longitude;
    }

    function getLatitude() {
        return $this->latitude;
    }

    function setLongitude($longitude) {
        $this->longitude = $longitude;
    }

    function setLatitude($latitude) {
        $this->latitude = $latitude;
    }

    
    public function __construct(array $array) {
        $this->setLongitude($array[self::KEY_LONGITUDE]);
        $this->setLatitude($array[self::KEY_LATITUDE]);
    }

    public function toArray(): array {
        return [
            self::KEY_LATITUDE  => $this->getLatitude(),
            self::KEY_LONGITUDE => $this->getLongitude()
        ];
    }

}
