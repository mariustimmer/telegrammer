<?php

namespace telegrammer\openweathermap;

use \telegrammer\JSONable AS JSONable;

class Weather extends JSONable {

    const KEY_DATETIME    = 'dt';
    const KEY_MAIN        = 'main';
    const KEY_TEMP        = 'temp';
    const KEY_TEMP_MIN    = 'temp_min';
    const KEY_TEMP_MAX    = 'temp_max';
    const KEY_PRESSURE    = 'pressure';
    const KEY_HUMIDITY    = 'humidity';
    const KEY_WEATHER     = 'weather';
    const KEY_DESCRIPTION = 'description';
    const KEY_WIND        = 'wind';
    const KEY_SPEED       = 'speed';

    private $dt;
    private $temp;
    private $temp_min;
    private $temp_max;
    private $pressure;
    private $humidity;
    private $description;
    private $speed;

    function getDT() {
        return $this->dt;
    }

    function getTemp() {
        return $this->temp;
    }

    function getTempMin() {
        return $this->temp_min;
    }

    function getTempMax() {
        return $this->temp_max;
    }

    function getPressure() {
        return $this->pressure;
    }

    function getHumidity() {
        return $this->humidity;
    }

    function getDescription() {
        return $this->description;
    }

    function getSpeed() {
        return $this->speed;
    }

    function setDT($dt) {
        $this->dt = $dt;
    }

    function setTemp($temp) {
        $this->temp = $temp;
    }

    function setTempMin($temp_min) {
        $this->temp_min = $temp_min;
    }

    function setTempMax($temp_max) {
        $this->temp_max = $temp_max;
    }

    function setPressure($pressure) {
        $this->pressure = $pressure;
    }

    function setHumidity($humidity) {
        $this->humidity = $humidity;
    }

    function setDescription($description) {
        $this->description = $description;
    }

    function setSpeed($speed) {
        $this->speed = $speed;
    }

    
    public function __construct(array $array) {
        $this->dt = $array[self::KEY_DATETIME];
        $this->temp = $array[self::KEY_MAIN][self::KEY_TEMP];
        $this->temp_min = $array[self::KEY_MAIN][self::KEY_TEMP_MIN];
        $this->temp_max = $array[self::KEY_MAIN][self::KEY_TEMP_MAX];
        $this->pressure = $array[self::KEY_MAIN][self::KEY_PRESSURE];
        $this->humidity = $array[self::KEY_MAIN][self::KEY_HUMIDITY];
        $this->description = $array[self::KEY_WEATHER][0][self::KEY_DESCRIPTION];
        $this->speed = $array[self::KEY_WIND][self::KEY_SPEED];
    }

    public function toArray(): array {
        return [
            self::KEY_DATETIME => $this->dt,
            self::KEY_MAIN     => [
                self::KEY_TEMP     => $this->temp,
                self::KEY_TEMP_MIN => $this->temp_min,
                self::KEY_TEMP_MAX => $this->temp_max,
                self::KEY_PRESSURE => $this->pressure,
                self::KEY_HUMIDITY => $this->humidity
            ],
            self::KEY_WEATHER => [
                [
                    self::KEY_DESCRIPTION => $this->description
                ]
            ],
            self::KEY_WIND     => [
                self::KEY_SPEED    => $this->speed
            ]
        ];
    }

}
