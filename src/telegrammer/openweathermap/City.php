<?php

namespace telegrammer\openweathermap;

use \telegrammer\JSONable AS JSONable;

class City extends JSONable {

    const KEY_ID          = 'id';
    const KEY_NAME        = 'name';
    const KEY_COORDINATES = 'coord';
    const KEY_COUNTRY     = 'country';
    const KEY_POPULATION  = 'population';
    const KEY_TIMEZONE    = 'timezone';

    private $id;
    private $name;
    private $coordinates;
    private $country;
    private $population;
    private $timezone;
    
    function getID() {
        return $this->id;
    }

    function getName() {
        return $this->name;
    }

    function getCoordinates(): Coordinates {
        return $this->coordinates;
    }

    function getCountry() {
        return $this->country;
    }

    function getPopulation() {
        return $this->population;
    }

    function getTimezone() {
        return $this->timezone;
    }

    function setName($name) {
        $this->name = $name;
    }

    function setCoordinates(Coordinates $coordinates) {
        $this->coordinates = $coordinates;
    }

    function setCountry($country) {
        $this->country = $country;
    }

    function setPopulation($population) {
        $this->population = $population;
    }

    function setTimezone($timezone) {
        $this->timezone = $timezone;
    }

    public function __construct(array $array) {
        $this->id          = $array[self::KEY_ID];
        $this->name        = $array[self::KEY_NAME];
        $this->coordinates = (!empty($array[self::KEY_COORDINATES])) ? new Coordinates($array[self::KEY_COORDINATES]) : NULL;
        $this->country     = $array[self::KEY_COUNTRY];
        $this->population  = isset($array[self::KEY_POPULATION]) ? $array[self::KEY_POPULATION] : 0;
        $this->timezone    = isset($array[self::KEY_TIMEZONE]) ? $array[self::KEY_TIMEZONE] : 0;
    }

    public function toArray(): array {
        return [
            self::KEY_ID          => $this->getID(),
            self::KEY_NAME        => $this->getName(),
            self::KEY_COORDINATES => $this->getCoordinates()->toArray(),
            self::KEY_COUNTRY     => $this->getCountry(),
            self::KEY_POPULATION  => $this->getPopulation(),
            self::KEY_TIMEZONE    => $this->getTimezone()
        ];
    }

}
