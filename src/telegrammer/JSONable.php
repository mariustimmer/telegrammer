<?php

namespace telegrammer;

abstract class JSONable {

    abstract public function __construct(array $array);

    abstract public function toArray(): array;

}
