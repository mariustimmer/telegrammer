<?php

namespace telegrammer;

USE \Exception AS Exception;

class Configuration {

    const FILENAME = ROOT_DIRECTORY . DIRECTORY_SEPARATOR .'config.ini';

    /**
     * Configuration data.
     * @var array
     */
    private $data;
    private static $instance;

    private function __construct() {
        $this->load();
    }

    /**
     * Loads the data from the configuration file.
     * @return bool
     * @throws Exception
     */
    public function load(): bool {
        if (!file_exists(self::FILENAME)) {
            throw new Exception(
                sprintf(
                    gettext("There is no configuration file '%s'!"),
                    self::FILENAME
                )
            );
        } else if (!is_readable(self::FILENAME)) {
            throw new Exception(
                sprintf(
                    gettext("The configuration file '%s' is not readable!"),
                    self::FILENAME
                )
            );
        }
        $this->data = parse_ini_file(self::FILENAME, true, INI_SCANNER_TYPED);
        if ($this->data === false) {
            throw new Exception(gettext("Could not read configuration file for no reason"));
        }
        return true;
    }

    /**
     * Returns the requested value from the configuration file.
     * @param string $section requested section in the ini file
     * @param string $key requested key within the given section
     * @return string
     */
    public function get(string $section, string $key): string {
        $value = '';
        if ((isset($this->data[$section])) &&
            (isset($this->data[$section][$key]))) {
            $value = $this->data[$section][$key];
        }
        return $value;
    }

    /**
     * Generates a new instance or uses the old one.
     * @return \telegrammer\Configuration
     */
    public static function getInstance(): Configuration {
        if (!self::$instance) {
            self::$instance = new Configuration();
        }
        return self::$instance;
    }

}
