<?php

namespace telegrammer\database;

use \PDO AS PDO;
use \telegrammer\Configuration AS Configuration;

class Connection {

    /**
     *
     * @var Connection
     */
    private static $instance;

    /**
     * Database connection
     * @var PDO
     */
    private $dbh;

    /**
     * Returns the static database handler or generates it.
     * @return \telegrammer\database\Connection
     */
    public static function getInstance(): Connection {
        if (!self::$instance) {
            self::$instance = new Connection();
        }
        return self::$instance;
    }

    private function __construct() {
        $dbtype = Configuration::getInstance()->get('Database', 'Type');
        if (self::isFlynn()) {
            /**
             * Connect to the database as Flynns terminal client user
             */
            $dsn = Configuration::getInstance()->get('Database', 'ConnectionStringFlynn');
        } else {
            /**
             * Connect to the database using the apache user.
             */
            $dsn = Configuration::getInstance()->get('Database', 'ConnectionStringApache');
        }
        $this->dbh = new PDO($dbtype .':'. $dsn);
        $this->dbh->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
        if (($dbtype === 'sqlite') && (!file_exists($dsn))) {
            $this->executeScript(ROOT_DIRECTORY .'/var/initdb.sql');
        }
    }

    /**
     * Finds out wheather we are running as a terminal script or via apache.
     * @return bool True if running in terminal or false
     */
    public static function isFlynn(): bool {
        return (strpos(php_sapi_name(), 'apache') === false);
    }

    /**
     * Executes a sql script described by $scriptname.
     * @param string $scriptname
     * @throws Exception
     */
    public function executeScript(string $scriptname) {
        $content = file_get_contents($scriptname);
        if ($content === false) {
            throw new \Exception(gettext("Could not find sql script!"));
        }
        $statements = explode(';', $content);
        try {
            $this->beginTransaction();
            foreach ($statements AS $statement) {
                if (empty(trim($statement))) {
                    continue;
                }
                $this->exec($statement);
            }
            $this->commit();
        } catch (\Exception $exception) {
            $this->rollBack();
            throw $exception;
        }
        
    }

    /**
     * Prepares a statement and executes it with the given parameters
     * in the assoc array.
     * @param string $query
     * @param array $data
     * @return array
     */
    public function executeQuery(string $query, array $data = []) {
        $statement = $this->dbh->prepare($query);
        $bindings = [];
        foreach ($data AS $placeholder => &$value) {
            if (is_array($value)) {
                trigger_error('Placeholder: :'. $placeholder);
                trigger_error($query);
            }
            $statement->bindParam(':'. $placeholder, $value);
        }
        $statement->execute();
        return $statement->fetchAll(PDO::FETCH_ASSOC);
    }

    public function exec(string $query) {
        return $this->dbh->exec($query);
    }

    public function beginTransaction() {
        return $this->dbh->beginTransaction();
    }

    public function commit() {
        return $this->dbh->commit();
    }

    public function rollBack() {
        return $this->dbh->rollBack();
    }

}
