<?php

namespace telegrammer\database;

use \telegrammer\openweathermap\City AS City;
use \telegrammer\openweathermap\Forecast AS Forecast;
use \telegrammer\openweathermap\Weather AS Weather;
use \telegrammer\telegram\User AS User;

class WeatherManager implements Deprovisionable {

    private static $instance;

    public static function getInstance(): WeatherManager {
        if (!self::$instance) {
            self::$instance = new WeatherManager();
        }
        return self::$instance;
    }

    public function addForecast(Forecast $forecast) {
        CityManager::getInstance()->upsert($forecast->getCity());
        foreach ($forecast->getList() AS $weather) {
            $this->addWeather($forecast->getCity(), $weather);
        }
    }

    public function addWeather(City $city, Weather $weather) {
        $query = 'INSERT INTO weather_forecast (city, dt, temp, temp_min, temp_max, pressure, humidity, description, wind, added) VALUES (:city, :dt, :temp, :temp_min, :temp_max, :pressure, :humidity, :description, :speed, CURRENT_TIMESTAMP);';
        $params = [
                'city'        => $city->getID(),
                'dt'          => date('Y-m-d H:i:s', $weather->getDT()),
                'temp'        => $weather->getTemp(),
                'temp_min'    => $weather->getTempMin(),
                'temp_max'    => $weather->getTempMax(),
                'pressure'    => $weather->getPressure(),
                'humidity'    => $weather->getHumidity(),
                'description' => $weather->getDescription(),
                'speed'       => $weather->getSpeed()
            ];
        Connection::getInstance()->executeQuery(
            $query,
            $params
        );
    }

    /**
     * Reads out which user wants a forecast for which city.
     * The result is a two dimensional assoc. array with the following keys:
     *  - userid (also chat_id)
     *  - first_name
     *  - username
     *  - cityid
     *  - name (of city)
     *  - country
     * @return array
     */
    public function getObservators() {
        $query = 'SELECT userid, first_name, username, cityid, name, country FROM weather_observations LEFT JOIN cities ON (weather_observations.cityid = cities.id) LEFT JOIN users ON (weather_observations.userid = users.id);';
        return Connection::getInstance()->executeQuery($query);
    }

    /**
     * Reads out which cities a certain user wants to observe.
     * The result is a two dimensional assoc. array with the following keys:
     *  - userid (also chat_id)
     *  - first_name
     *  - username
     *  - cityid
     *  - name (of city)
     *  - country
     * @param User $user
     * @return array
     */
    public function getObservedCitiesOfUser(User $user) {
        $query = 'SELECT userid, first_name, username, cityid, name, country FROM weather_observations LEFT JOIN cities ON (weather_observations.cityid = cities.id) LEFT JOIN users ON (weather_observations.userid = users.id) WHERE userid = :userid;';
        return Connection::getInstance()->executeQuery(
            $query,
            [
                'userid' => $user->getID()
            ]
        );
    }

    /**
     * Checks the time for the next available forecast
     * @param City $city
     * @return string Timestamp as string
     * @throws \Exception
     */
    public function getNextAvailableForecastTime(City $city): string {
        $find_time_query = 'SELECT dt, CURRENT_TIMESTAMP-dt AS diff FROM weather_forecast WHERE city = :id AND dt >= CURRENT_TIMESTAMP GROUP BY dt ORDER BY CURRENT_TIMESTAMP-dt DESC LIMIT 1;';
        $find_time_param = [
            'id' => intval($city->getID())
        ];
        $find_time_result = Connection::getInstance()->executeQuery($find_time_query, $find_time_param);
        if (count($find_time_result) !== 1) {
            throw new \Exception(
                sprintf(
                    gettext("No forecast available for %s [%d]"),
                    $city->getName(),
                    $city->getID()
                )
            );
        }
        return $find_time_result[0]['dt'];
    }

    /**
     * Reads out all forecasts for the available time for the requested city.
     * @param City $city
     * @throws \Exception
     */
    public function getNextForecastSummary(City $city) {
        $query = 'SELECT id, city, dt, temp, temp_min, temp_max, pressure, humidity, description, wind, added FROM weather_forecast WHERE city = 2881973 AND dt = :dt ORDER BY added;';
        $param = [
            'dt' => $this->getNextAvailableForecastTime($city)
        ];
        $results = Connection::getInstance()->executeQuery($query, $param);
        $summary = [];
        foreach ($results AS $forecast) {
            $added_rounded = date('Y-m-d H', strtotime($forecast['added']));
            $summary[$added_rounded] = [
                'id'                     => $forecast['id'],
                'city'                   => $forecast['city'],
                Weather::KEY_DATETIME    => $forecast['dt'],
                Weather::KEY_TEMP        => $forecast['temp'],
                Weather::KEY_TEMP_MIN    => $forecast['temp_min'],
                Weather::KEY_TEMP_MAX    => $forecast['temp_max'],
                Weather::KEY_PRESSURE    => $forecast['pressure'],
                Weather::KEY_HUMIDITY    => $forecast['humidity'],
                Weather::KEY_DESCRIPTION => $forecast['description'],
                Weather::KEY_WIND        => $forecast['wind'],
                'added'                  => $forecast['added']
            ];
        }
        return $summary;
    }

    /**
     * Returns the latest forecast for the given city and the date.
     * @param City $city
     * @param string $date Date of the requested forecast
     * @throws \Exception
     */
    public function getForecastFor(City $city, string $date = '') {
        if (empty(trim($date))) {
            $date = date('Y-m-d');
        }
        $query = "SELECT id, city, dt, temp, temp_min, temp_max, pressure, humidity, description, wind, added FROM weather_forecast WHERE id IN (SELECT MAX(id) FROM weather_forecast WHERE DATE(dt) = :date AND city = :city GROUP BY dt) ORDER BY dt;";
        $param = [
            'date' => $date,
            'city' => $city->getID()
        ];
        $results = Connection::getInstance()->executeQuery($query, $param);
        $summary = [];
        foreach ($results AS $forecast) {
            $summary[strtotime($forecast['dt'])] = [
                'id'                     => $forecast['id'],
                'city'                   => $forecast['city'],
                Weather::KEY_DATETIME    => $forecast['dt'],
                Weather::KEY_TEMP        => $forecast['temp'],
                Weather::KEY_TEMP_MIN    => $forecast['temp_min'],
                Weather::KEY_TEMP_MAX    => $forecast['temp_max'],
                Weather::KEY_PRESSURE    => $forecast['pressure'],
                Weather::KEY_HUMIDITY    => $forecast['humidity'],
                Weather::KEY_DESCRIPTION => $forecast['description'],
                Weather::KEY_WIND        => $forecast['wind'],
                'added'                  => $forecast['added']
            ];
        }
        return $summary;
    }

    public function deprovisioning()
    {
        $query = "DELETE FROM weather_forecast WHERE dt < CURRENT_TIMESTAMP AND DATE_PART('hours', dt) >= 3;";
        Connection::getInstance()->exec($query);
    }

}
