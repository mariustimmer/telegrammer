<?php

namespace telegrammer\database;

use \telegrammer\telegram\User;

class UserManager {

    /**
     *
     * @var UserManager
     */
    private static $instance;

    public static function getInstance(): UserManager {
        if (!self::$instance) {
            self::$instance = new UserManager();
        }
        return self::$instance;
    }

    /**
     * Returns the id of the given role name.
     * @param string $name
     * @return int
     */
    public function getRoleID(string $name) {
        $query = 'SELECT id FROM roles WHERE name = :name;';
        $result = Connection::getInstance()->executeQuery(
            $query,
            [
                'name' => $name
            ]
        );
        if (count($result) !== 1) {
            return null;
        }
        return intval($result[0]['id']);
    }

    /**
     * Returns a list of all users of a role.
     * @param int|string $role ID or name of the role
     * @return User[]
     */
    public function getUsersByRole($role) {
        if (is_string($role)) {
            $role = $this->getRoleID($role);
        }
        if (!is_int($role)) {
            return null;
        }
        $query = 'SELECT userid FROM user_roles WHERE roleid = :roleid;';
        $result = Connection::getInstance()->executeQuery(
            $query,
            [
                'roleid' => $role
            ]
        );
        $users = [];
        foreach ($result AS $row) {
            array_push($users, $this->getUserByID($row['userid']));
        }
        return $users;
    }

    /**
     * 
     * @param int $user_id
     * @return User
     */
    public function getUserByID(int $user_id) {
        $query = 'SELECT id, first_name, last_name, username, language_code FROM users WHERE id = :id;';
        $result = Connection::getInstance()->executeQuery(
            $query,
            [
                'id' => $user_id
            ]
        );
        if (count($result) !== 1) {
            return null;
        }
        return new User($result[0]);
    }

    /**
     * 
     * @return int[]
     */
    public function getAllUserIDs(): array {
        $query = 'SELECT id FROM users;';
        $result = Connection::getInstance()->executeQuery(
            $query
        );
        $ids = [];
        foreach ($result AS $row) {
            array_push($ids, $row[User::KEY_ID]);
        }
        return $ids;
    }

    public function upsertUser(User $user) {
        print __FUNCTION__ . PHP_EOL;
        $query = "INSERT INTO users (id, first_name, last_name, username, language_code, added) VALUES (:id, :first_name, :last_name, :username, :language_code, CURRENT_TIMESTAMP);";
        if ($this->getUserByID($user->getID())) {
            $query = "UPDATE users SET first_name = :first_name, last_name = :last_name, username = :username, language_code = :language_code WHERE id = :id;";
        }
        $params = $user->toArray();
        unset($params[User::KEY_IS_BOT]);
        Connection::getInstance()->executeQuery(
            $query,
            $params
        );
    }

}
