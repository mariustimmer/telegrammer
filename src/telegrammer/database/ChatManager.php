<?php

namespace telegrammer\database;

use \telegrammer\telegram\Chat;
use \telegrammer\telegram\Message;
use \telegrammer\telegram\Update;
use \telegrammer\telegram\Voice;

class ChatManager {

    /**
     *
     * @var ChatManager
     */
    private static $instance;

    public static function getInstance(): ChatManager {
        if (!self::$instance) {
            self::$instance = new ChatManager();
        }
        return self::$instance;
    }

    /**
     * 
     * @param int $chat_id
     * @return Chat
     */
    public function getChatByID(int $chat_id) {
        $query = 'SELECT id, type, title, all_members_are_admins, description FROM chats WHERE id = :id;';
        $result = Connection::getInstance()->executeQuery(
            $query,
            [
                'id' => $chat_id
            ]
        );
        if (count($result) !== 1) {
            return null;
        }
        return new Chat($result[0]);
    }

    public function upsertChat(Chat $chat) {
        $query = "INSERT INTO chats (id, type, title, all_members_are_admins, description, added) VALUES (:id, :type, :title, :all_members_are_admins, :description, CURRENT_TIMESTAMP);";
        if ($this->getChatByID($chat->getID())) {
            $query = "UPDATE chats SET type = :type, title = :title, all_members_are_admins = :all_members_are_admins, description = :description WHERE id = :id;";
        }
        $params = [
            Chat::KEY_ID                     => intval($chat->getID()),
            Chat::KEY_TYPE                   => strval($chat->getType()),
            Chat::KEY_TITLE                  => strval($chat->getTitle()),
            Chat::KEY_ALL_MEMBERS_ARE_ADMINS => intval(boolval(intval($chat->getAllMembersAreAdmins()))),
            Chat::KEY_DESCRIPTION            => strval($chat->getDescription())
        ];
        Connection::getInstance()->executeQuery(
            $query,
            $params
        );
    }

    /**
     * 
     * @param int $message_id
     * @return Message
     */
    public function getMessageByID(int $message_id) {
        $query = 'SELECT id AS message_id, from_id, date, chat_id, reply_to_message_id, edit_date, text, voice FROM messages WHERE id = :message_id;';
        $result = Connection::getInstance()->executeQuery(
            $query,
            [
                'message_id' => $message_id
            ]
        );
        if (count($result) !== 1) {
            return null;
        }
        $result = $result[0];
        if ((isset($result['from_id'])) &&
            ($result['from_id'] > 0)) {
            $result[Message::KEY_FROM] = UserManager::getInstance()->getUserByID(intval($result['from_id']))->toArray();
        }
        if ((isset($result['chat_id'])) &&
            ($result['chat_id'] > 0)) {
            $result[Message::KEY_CHAT] = $this->getChatByID(intval($result['chat_id']))->toArray();
        }
        if ((isset($result['reply_to_message_id'])) &&
            ($result['reply_to_message_id'] > 0)) {
            $result[Message::KEY_REPLY_TO_MESSAGE] = $this->getMessageByID(intval($result['reply_to_message_id']))->toArray();
        }
        return new Message($result);
    }

    public function upsertMessage(Message $message) {
        if (($message->getFrom()) &&
            (!UserManager::getInstance()->getUserByID($message->getFrom()->getID()))) {
            UserManager::getInstance()->upsertUser($message->getFrom());
        }
        if (($message->getChat()) &&
            (!$this->getChatByID($message->getChat()->getID()))) {
            $this->upsertChat($message->getChat());
        }
        if (($message->getReplyToMessage()) &&
            (!$this->getMessageByID($message->getReplyToMessage()->getID()))) {
            $this->upsertMessage($message->getReplyToMessage());
        }
        $query = "INSERT INTO messages (id, from_id, date, chat_id, reply_to_message_id, edit_date, text, voice, added) VALUES (:message_id, :from, :date, :chat, :reply_to_message, :edit_date, :text, :voice, CURRENT_TIMESTAMP);";
        $params = $message->toArray();
        if (!empty($params[Message::KEY_FROM])) {
            $params[Message::KEY_FROM] = $message->getFrom()->getID();
        }
        if (!empty($params[Message::KEY_CHAT])) {
            $params[Message::KEY_CHAT] = $message->getChat()->getID();
        }
        if (!empty($params[Message::KEY_REPLY_TO_MESSAGE])) {
            $params[Message::KEY_REPLY_TO_MESSAGE] = $message->getReplyToMessage()->getID();
        } else {
            $params[Message::KEY_REPLY_TO_MESSAGE] = null;
        }
        if (empty($params[Message::KEY_VOICE])) {
            $params[Message::KEY_VOICE] = null;
        }
        $params['date'] = date('Y-m-d H:i:s', $params['date']);
        if ($this->getMessageByID($message->getID())) {
            $params = [
                'edit_date' => date('Y-m-d H:i:s', $message->getEditDate()),
                'text'      => $message->getText(),
                'voice'     => $message->getVoice()->getFileID(),
                'message_id'        => $message->getID()
            ];
            $query = "UPDATE messages SET edit_date = :edit_date, text = :text, voice = :voice WHERE id = :message_id;";
        }
        if ((is_array($params[Message::KEY_VOICE])) &&
            (isset($params[Message::KEY_VOICE][Voice::KEY_FILE]))) {
            /**
             * In case we want to store a voice we will just store the file id.
             */
            $params[Message::KEY_VOICE] = $params[Message::KEY_VOICE][Voice::KEY_FILE];
        }
        Connection::getInstance()->executeQuery(
            $query,
            $params
        );
    }

    /**
     * 
     * @param int $update_id
     * @return Update
     */
    public function getUpdateByID(int $update_id) {
        $query = 'SELECT id AS update_id, message_id, edited_message_id FROM updates WHERE id = :update_id;';
        $result = Connection::getInstance()->executeQuery(
            $query,
            [
                'update_id' => $update_id
            ]
        );
        if (count($result) !== 1) {
            return null;
        }
        $result = $result[0];
        if ((isset($result['message_id'])) &&
            ($result['message_id'] > 0)) {
            $result[Update::KEY_MESSAGE] = $this->getMessageByID(intval($result['message_id']))->toArray();
        }
        if ((isset($result['edited_message_id'])) &&
            ($result['edited_message_id'] > 0)) {
            $result[Update::KEY_EDITED_MESSAGE] = $this->getMessageByID(intval($result['edited_message_id']))->toArray();
        }
        return new Update($result);
    }

    public function upsertUpdate(Update $update) {
        if (($update->getMessage()) &&
            (!$this->getMessageByID($update->getMessage()->getID()))) {
            $this->upsertMessage($update->getMessage());
        }
        if (($update->getEditedMessage()) &&
            (!$this->getMessageByID($update->getEditedMessage()->getID()))) {
            $this->upsertMessage($update->getEditedMessage());
        }
        $params = [
            Update::KEY_ID             => intval($update->getID()),
            Update::KEY_MESSAGE        => null,
            Update::KEY_EDITED_MESSAGE => null
        ];
        $query = "INSERT INTO updates (id, message_id, edited_message_id, added) VALUES (:update_id, :message, :edited_message, CURRENT_TIMESTAMP);";
        if ($this->getUpdateByID($update->getID())) {
            $query = "UPDATE updates SET message_id = :message, edited_message_id = :edited_message WHERE id = :update_id;";
        }
        if (!empty($params[Update::KEY_MESSAGE])) {
            $params[Update::KEY_MESSAGE] = intval($update->getMessage()->getID());
        }
        if (!empty($params[Update::KEY_EDITED_MESSAGE])) {
            $params[Update::KEY_EDITED_MESSAGE] = intval($update->getEditedMessage()->getID());
        }
        Connection::getInstance()->executeQuery(
            $query,
            $params
        );
    }

    /**
     * Fetches all messages within a conversation from the database.
     * @param int $chat_id
     * @return Message[]
     */
    public function getConversation(int $chat_id) {
        $conversation = [];
        $query = 'SELECT id FROM messages WHERE chat_id = :chat_id ORDER BY date;';
        $result = Connection::getInstance()->executeQuery(
            $query,
            [
                'chat_id' => $chat_id
            ]
        );
        foreach ($result AS $row) {
            array_push($conversation, $this->getMessageByID(intval($row['id'])));
        }
        return $conversation;
    }

}
