<?php

namespace telegrammer\database;

interface Deprovisionable
{

    /**
     * Deprovisioning method.
     */
    public function deprovisioning();

}
