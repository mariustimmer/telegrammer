<?php

namespace telegrammer\database;

use \telegrammer\openweathermap\City AS City;

class CityManager {

    /**
     *
     * @var CityManager
     */
    private static $instance;

    public static function getInstance(): CityManager {
        if (!self::$instance) {
            self::$instance = new CityManager();
        }
        return self::$instance;
    }

    /**
     * Reads out the ID of a city name.
     * @param string $name
     * @return int
     */
    public function getCityID(string $name) {
        $query = 'SELECT id FROM cities WHERE name = :name;';
        $response = Connection::getInstance()->executeQuery(
            $query,
            [
                'name' => $name
            ]
        );
        if (count($response) !== 1) {
            return null;
        }
        return $response[0]['id'];
    }

    /**
     * 
     * @param int $id
     * @return City
     */
    public function getCity(int $id) {
        $query = 'SELECT id, name, country, coordinates, population, timezone FROM cities WHERE id = :id;';
        $cities = Connection::getInstance()->executeQuery(
            $query,
            [
                'id' => $id
            ]
        );
        if (count($cities) === 0) {
            return null;
        }
        $city = $cities[0];
        if (!empty($city['coordinates'])) {
            $coord_query = 'SELECT longitude AS lon, latitude AS lat FROM coordinates WHERE id = :id;';
            $coord_result = Connection::getInstance()->executeQuery(
                $coord_query,
                [
                    'id' => intval($city['coordinates'])
                ]
            );
        } else {
            // There is no coordinate
            $coord_result = [NULL];
        }
        $city[City::KEY_COORDINATES] = $coord_result[0];
        return new City($city);
    }

    public function addCity(City $city) {
        if ($this->getCity($city->getID()) !== null) {
            return null;
        }
        $coord_query_get = 'SELECT id FROM coordinates WHERE longitude = :longitude AND latitude = :latitude;';
        $coord_result_get = Connection::getInstance()->executeQuery(
            $coord_query_get,
            [
                'longitude' => intval($city->getCoordinates()->getLongitude()),
                'latitude'  => intval($city->getCoordinates()->getLatitude())
            ]
        );
        if (count($coord_result_get) === 0) {
            $coord_query = 'INSERT INTO coordinates (longitude, latitude, added) VALUES (:lon, :lat, CURRENT_TIMESTAMP);';
            Connection::getInstance()->executeQuery(
                $coord_query,
                [
                    'lon' => $city->getCoordinates()->getLongitude(),
                    'lat' => $city->getCoordinates()->getLatitude()
                ]
            );
        }
        $query = 'INSERT INTO cities (id, name, country, coordinates, population, timezone, added) VALUES (:id, :name, :country, (SELECT id FROM coordinates WHERE longitude = :lon AND latitude = :lat LIMIT 1), :population, :timezone, CURRENT_TIMESTAMP);';
        Connection::getInstance()->executeQuery(
            $query,
            [
                'id'         => $city->getID(),
                'name'       => $city->getName(),
                'country'    => $city->getCountry(),
                'lon'        => $city->getCoordinates()->getLongitude(),
                'lat'        => $city->getCoordinates()->getLatitude(),
                'population' => $city->getPopulation(),
                'timezone'   => $city->getTimezone(),
            ]
        );
        return true;
    }

    public function upsert(City $city): bool {
        $this->addCity($city);
        $query = 'UPDATE cities SET name = :name, country = :country, population = :population, timezone = :timezone WHERE id = :id;';
        $result = Connection::getInstance()->executeQuery(
            $query,
            [
                'name'       => $city->getName(),
                'country'    => $city->getCountry(),
                'population' => $city->getPopulation(),
                'timezone'   => $city->getTimezone(),
                'id'         => $city->getID()
            ]
        );
        return boolval($result);
    }

}
