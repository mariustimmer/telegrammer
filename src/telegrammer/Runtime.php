<?php

namespace telegrammer;

class Runtime {

    /**
     * Forks the process.
     * @return bool True within the parent of false if this is a child
     * @throws Exception
     */
    public static function startNewProcess(): bool {
        $pid = pcntl_fork();
        if ($pid === -1) {
            throw new Exception(gettext("Could not fork to background"));
        }
        if (!boolval($pid)) {
            posix_setsid();
        }
        return boolval($pid);
    }

}
