<?php

namespace telegrammer\telegram;

use \telegrammer\JSONable AS JSONable;

class User extends JSONable {

    const KEY_ID            = 'id';
    const KEY_IS_BOT        = 'is_bot';
    const KEY_FIRST_NAME    = 'first_name';
    const KEY_LAST_NAME     = 'last_name';
    const KEY_USERNAME      = 'username';
    const KEY_LANGUAGE_CODE = 'language_code';

    /**
     * Unique identifier for this user or bot
     * @var int
     */
    private $id;

    /**
     * True, if this is a bot
     * @var bool
     */
    private $is_bot;

    /**
     * User's or bot's first name
     * @var string
     */
    private $first_name;

    /**
     * (Optional) User's or bot's last name
     * @var string
     */
    private $last_name;

    /**
     * (Optional) User's or bot's username
     * @var string
     */
    private $username;

    /**
     * (Optional) IETF language tag
     * @var string
     */
    private $language_code;

    function getID() {
        return $this->id;
    }

    function getIsBot() {
        return $this->is_bot;
    }

    function getFirstName() {
        return $this->first_name;
    }

    function getLastName() {
        return $this->last_name;
    }

    function getUsername() {
        return $this->username;
    }

    function getLanguageCode() {
        return $this->language_code;
    }

    function setIsBot($is_bot) {
        $this->is_bot = $is_bot;
    }

    function setFirstName($first_name) {
        $this->first_name = $first_name;
    }

    function setLastName($last_name) {
        $this->last_name = $last_name;
    }

    function setUsername($username) {
        $this->username = $username;
    }

    function setLanguageCode($language_code) {
        $this->language_code = $language_code;
    }

    public function __construct(array $array) {
        $this->id            = $array[self::KEY_ID];
        $this->is_bot        = isset($array[self::KEY_IS_BOT]) ? $array[self::KEY_IS_BOT] : false;
        $this->first_name    = $array[self::KEY_FIRST_NAME];
        $this->last_name     = (!empty($array[self::KEY_LAST_NAME])) ? $array[self::KEY_LAST_NAME] : '';
        $this->username      = (!empty($array[self::KEY_USERNAME])) ? $array[self::KEY_USERNAME] : '';
        $this->language_code = (!empty($array[self::KEY_LANGUAGE_CODE])) ? $array[self::KEY_LANGUAGE_CODE] : '';
    }

    public function toArray(): array {
        return [
            self::KEY_ID            => $this->getID(),
            self::KEY_IS_BOT        => $this->getIsBot(),
            self::KEY_FIRST_NAME    => $this->getFirstName(),
            self::KEY_LAST_NAME     => $this->getLastName(),
            self::KEY_USERNAME      => $this->getUsername(),
            self::KEY_LANGUAGE_CODE => $this->getLanguageCode()
        ];
    }

}
