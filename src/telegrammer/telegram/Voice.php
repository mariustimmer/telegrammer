<?php

namespace telegrammer\telegram;

use \telegrammer\JSONable AS JSONable;

class Voice extends JSONable
{

    const KEY_FILE     = 'file_id';
    const KEY_DURATION = 'duration';
    const KEY_TYPE     = 'mime_type';
    const KEY_SIZE     = 'file_size';

    private $file_id;
    private $duration;
    private $mime_type;
    private $file_size;

    function getFileID()
    {
        return $this->file_id;
    }

    function getDuration()
    {
        return $this->duration;
    }

    function getMimeType()
    {
        return $this->mime_type;
    }

    function getFileSize()
    {
        return $this->file_size;
    }

    function setFileID($file_id)
    {
        $this->file_id = $file_id;
    }

    function setDuration($duration)
    {
        $this->duration = $duration;
    }

    function setMimeType($mime_type)
    {
        $this->mime_type = $mime_type;
    }

    function setFileSize($file_size)
    {
        $this->file_size = $file_size;
    }

    public function __construct(array $array)
    {
        $this->file_id  = $array[self::KEY_FILE];
        $this->duration = $array[self::KEY_DURATION];
        if (!empty($array[self::KEY_TYPE])) {
            $this->mime_type = $array[self::KEY_TYPE];
        }
        if (!empty($array[self::KEY_SIZE])) {
            $this->file_size = $array[self::KEY_SIZE];
        }
    }

    public function toArray(): array
    {
        return [
            self::KEY_FILE     => $this->getFileID(),
            self::KEY_DURATION => $this->getDuration(),
            self::KEY_TYPE     => ($this->mime_type) ? $this->getMimeType() : '',
            self::KEY_SIZE     => ($this->file_size) ? $this->getFileSize() : 0
        ];
    }

}
