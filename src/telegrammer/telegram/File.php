<?php

namespace telegrammer\telegram;

use \telegrammer\JSONable AS JSONable;

class File extends JSONable
{

    const KEY_ID   = 'file_id';
    const KEY_SIZE = 'file_size';
    const KEY_PATH = 'file_path';

    private $file_id;
    private $file_size;
    private $file_path;

    function getFileID()
    {
        return $this->file_id;
    }

    function getFileSize()
    {
        return $this->file_size;
    }

    function getFilePath()
    {
        return $this->file_path;
    }

    function setFileID($file_id)
    {
        $this->file_id = $file_id;
    }

    function setFileSize($file_size)
    {
        $this->file_size = $file_size;
    }

    function setFilePath($file_path)
    {
        $this->file_path = $file_path;
    }

    public function __construct(array $array)
    {
        $this->file_id   = $array[self::KEY_ID];
        if (!empty($array[self::KEY_SIZE])) {
            $this->file_size = $array[self::KEY_SIZE];
        }
        $this->file_path = $array[self::KEY_PATH];
    }

    public function toArray(): array
    {
        return [
            self::KEY_ID   => $this->getFileID(),
            self::KEY_SIZE => ($this->file_size) ? $this->getFileSize() : 0,
            self::KEY_PATH => $this->getFilePath()
        ];
    }

}
