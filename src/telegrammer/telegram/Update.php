<?php

namespace telegrammer\telegram;

use \telegrammer\JSONable AS JSONable;

class Update extends JSONable {

    const KEY_ID = 'update_id';
    const KEY_MESSAGE = 'message';
    const KEY_EDITED_MESSAGE = 'edited_message';

    /**
     *
     * @var int
     */
    private $id;

    /**
     *
     * @var Message
     */
    private $message;

    /**
     *
     * @var Message
     */
    private $edited_message;

    function getID() {
        return $this->id;
    }

    function getMessage() {
        return $this->message;
    }

    function getEditedMessage() {
        return $this->edited_message;
    }

    function setMessage(Message $message) {
        $this->message = $message;
    }

    function setEditedMessage(Message $edited_message) {
        $this->edited_message = $edited_message;
    }

    public function __construct(array $array) {
        $this->id = $array[self::KEY_ID];
        if (!empty($array[self::KEY_MESSAGE])) {
            $this->message = new Message($array[self::KEY_MESSAGE]);
        }
        if (!empty($array[self::KEY_EDITED_MESSAGE])) {
            $this->edited_message = new Message($array[self::KEY_EDITED_MESSAGE]);
        }
    }

    public function toArray(): array {
        return [
            self::KEY_ID             => $this->getID(),
            self::KEY_MESSAGE        => ($this->message) ?$this->getMessage()->toArray() : [],
            self::KEY_EDITED_MESSAGE => ($this->edited_message) ?$this->getEditedMessage()->toArray() : []
        ];
    }

}
