<?php

namespace telegrammer\telegram;

use \telegrammer\JSONable AS JSONable;

class Message extends JSONable {

    const KEY_ID               = 'message_id';
    const KEY_FROM             = 'from';
    const KEY_DATE             = 'date';
    const KEY_CHAT             = 'chat';
    const KEY_REPLY_TO_MESSAGE = 'reply_to_message';
    const KEY_EDIT_DATE        = 'edit_date';
    const KEY_TEXT             = 'text';
    const KEY_VOICE            = 'voice';

    private $id;
    private $from;
    private $date;
    private $chat;
    private $reply_to_message;
    private $edit_date;
    private $text;
    private $voice;

    function getID() {
        return $this->id;
    }

    /**
     * 
     * @return \telegrammer\telegram\User
     */
    function getFrom() {
        return $this->from;
    }

    function getDate() {
        return $this->date;
    }

    /**
     * 
     * @return \telegrammer\telegram\Chat
     */
    function getChat(): Chat {
        return $this->chat;
    }

    /**
     * 
     * @return \telegrammer\telegram\Message
     */
    function getReplyToMessage() {
        return $this->reply_to_message;
    }

    function getEditDate() {
        return $this->edit_date;
    }

    function getText() {
        return $this->text;
    }

    function getVoice() {
        return $this->voice;
    }

    function setFrom($from) {
        $this->from = $from;
    }

    function setDate($date) {
        $this->date = $date;
    }

    function setChat($chat) {
        $this->chat = $chat;
    }

    function setReplyToMessage($reply_to_message) {
        $this->reply_to_message = $reply_to_message;
    }

    function setEditDate($edit_date) {
        $this->edit_date = $edit_date;
    }

    function setText($text) {
        $this->text = $text;
    }

    function setVoice($voice) {
        $this->voice = $voice;
    }

    public function __construct(array $array) {
        $this->id                   = $array[self::KEY_ID];
        if (!empty($array[self::KEY_FROM])) {
            $this->from             = new User($array[self::KEY_FROM]);
        }
        $this->date                 = $array[self::KEY_DATE];
        if (!empty($array[self::KEY_CHAT])) {
            $this->chat             = new Chat($array[self::KEY_CHAT]);
        }
        if (!empty($array[self::KEY_REPLY_TO_MESSAGE])) {
            $this->reply_to_message = new Message($array[self::KEY_REPLY_TO_MESSAGE]);
        }
        if (!empty($array[self::KEY_EDIT_DATE])) {
            $this->edit_date = $array[self::KEY_EDIT_DATE];
        }
        if (!empty($array[self::KEY_TEXT])) {
            $this->text = $array[self::KEY_TEXT];
        }
        if (!empty($array[self::KEY_VOICE])) {
            $this->voice = new Voice($array[self::KEY_VOICE]);
        }
    }

    public function toArray(): array {
        return [
            self::KEY_ID               => $this->getID(),
            self::KEY_FROM             => ($this->from) ? $this->getFrom()->toArray() : [],
            self::KEY_DATE             => $this->getDate(),
            self::KEY_CHAT             => ($this->chat) ? $this->getChat()->toArray() : [],
            self::KEY_REPLY_TO_MESSAGE => ($this->reply_to_message) ? $this->getReplyToMessage()->toArray() : [],
            self::KEY_EDIT_DATE        => $this->getEditDate(),
            self::KEY_TEXT             => $this->getText(),
            self::KEY_VOICE            => ($this->voice) ? $this->getVoice()->toArray() : []
        ];
    }

}
