<?php

namespace telegrammer\telegram;

use \telegrammer\JSONable AS JSONable;

class Chat extends JSONable {

    const KEY_ID                     = 'id';
    const KEY_TYPE                   = 'type';
    const KEY_TITLE                  = 'title';
    const KEY_USERNAME               = 'username';
    const KEY_FIRST_NAME             = 'fist_name';
    const KEY_LAST_NAME              = 'last_name';
    const KEY_ALL_MEMBERS_ARE_ADMINS = 'all_members_are_admins';
    const KEY_DESCRIPTION            = 'description';

    private $id;
    private $type;
    private $title;
    private $username;
    private $first_name;
    private $last_name;
    private $all_members_are_admins;
    private $description;

    public function __construct(array $array) {
        $this->id                     = $array[self::KEY_ID];
        $this->type                   = $array[self::KEY_TYPE];
        $this->title                  = (!empty($array[self::KEY_TITLE])) ? $array[self::KEY_TITLE] : '';
        $this->username               = (!empty($array[self::KEY_USERNAME])) ? $array[self::KEY_USERNAME] : '';
        $this->first_name             = (!empty($array[self::KEY_FIRST_NAME])) ? $array[self::KEY_FIRST_NAME] : '';
        $this->last_name              = (!empty($array[self::KEY_LAST_NAME])) ? $array[self::KEY_LAST_NAME] : '';
        $this->all_members_are_admins = (!empty($array[self::KEY_ALL_MEMBERS_ARE_ADMINS])) ? $array[self::KEY_ALL_MEMBERS_ARE_ADMINS] : '';
        $this->description            = (!empty($array[self::KEY_DESCRIPTION])) ? $array[self::KEY_DESCRIPTION] : '';
    }

    public function toArray(): array {
        return [
            self::KEY_ID                     => $this->getID(),
            self::KEY_TYPE                   => $this->getType(),
            self::KEY_TITLE                  => $this->getTitle(),
            self::KEY_USERNAME               => $this->getUsername(),
            self::KEY_FIRST_NAME             => $this->getFirstName(),
            self::KEY_LAST_NAME              => $this->getLastName(),
            self::KEY_ALL_MEMBERS_ARE_ADMINS => $this->getAllMembersAreAdmins(),
            self::KEY_DESCRIPTION            => $this->getDescription()
        ];
    }

    function getID() {
        return $this->id;
    }

    function getType() {
        return $this->type;
    }

    function getTitle() {
        return $this->title;
    }

    function getUsername() {
        return $this->username;
    }

    function getFirstName() {
        return $this->first_name;
    }

    function getLastName() {
        return $this->last_name;
    }

    function getAllMembersAreAdmins() {
        return $this->all_members_are_admins;
    }

    function getDescription() {
        return $this->description;
    }

    function setType($type) {
        $this->type = $type;
    }

    function setTitle($title) {
        $this->title = $title;
    }

    function setUsername($username) {
        $this->username = $username;
    }

    function setFirstName($first_name) {
        $this->first_name = $first_name;
    }

    function setLastName($last_name) {
        $this->last_name = $last_name;
    }

    function setAllMembersAreAdmins($all_members_are_admins) {
        $this->all_members_are_admins = $all_members_are_admins;
    }

    function setDescription($description) {
        $this->description = $description;
    }

}
