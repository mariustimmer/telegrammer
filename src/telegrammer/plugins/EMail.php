<?php

namespace telegrammer\plugins;

class EMail {

    /**
     * E-Mail header.
     * @var array
     */
    private $header_fields;

    /**
     * E-Mail body.
     * @var string
     */
    private $body;

    public function __construct(array $header_fields, string $body) {
        $this->header_fields = $header_fields;
        $this->body = $body;
    }

    public function getFrom(): string {
        return $this->getHeaderField('from');
    }

    public function getFromName(): string {
        $from = $this->getFrom();
        if (strpos($from, ' <') !== false) {
            $parts = explode(' <', $from);
            if (!empty(trim($parts[0]))) {
                $from = trim($parts[0]);
            }
        }
        return $from;
    }

    public function getSubject(): string {
        return $this->getHeaderField('subject');
    }

    public function getRecipients(): array {
        $recipients = [];
        $raw_recipients = $this->getHeaderField('to');
        if (strpos($raw_recipients, ',') !== false) {
            foreach (explode(',', $raw_recipients) AS $recipient) {
                array_push($recipients, trim($recipient));
            }
        }
        return $recipients;
    }

    public function getMessageID(): string {
        $msg_id = trim($this->getHeaderField('message-id'));
        return substr(
            $msg_id,
            1,
            strlen($msg_id) - 2
        );
    }

    public function getHeaderField(string $name): string {
        if (isset($this->header_fields[$name])) {
            return $this->header_fields[$name];
        }
        return '';
    }

    public function getHeaderFields(): array {
        return $this->header_fields;
    }

    public function getBody(): string {
        return $this->body;
    }

}
