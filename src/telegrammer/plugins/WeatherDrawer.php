<?php

namespace telegrammer\plugins;

use \telegrammer\database\WeatherManager;
use \telegrammer\openweathermap\City;
use \telegrammer\plugins\Drawer;

class WeatherDrawer {

    const TEMP_WARM     = 23;
    const TEMP_HOT      = 28;
    const TEMP_COLD     = 15;
    const TEMP_FREEZING = 0;

    /**
     *
     * @var WeatherDrawer
     */
    private static $instance;

    public static function getInstance(): WeatherDrawer {
        if (!self::$instance) {
            self::$instance = new WeatherDrawer();
        }
        return self::$instance;
    }

    /**
     * Converts a temperature (in grad celsius) to a rgb value.
     * @param int $temperature
     * @return string
     */
    public static function temperatureToColor(int $temperature): string {
        if ($temperature >= self::TEMP_HOT) {
            return 'rgb(255,147,41)';
        } else if ($temperature >= self::TEMP_WARM) {
            return 'rgb(255,214,170)';
        } else if ($temperature <= self::TEMP_FREEZING) {
            return 'rgb(64,156,255)';
        } else if ($temperature <= self::TEMP_COLD) {
            return 'rgb(201,226,240)';
        }
        return 'rgb(0,5,11)';
    }

    public function generateForecastSummaryForCity(City $city): string {
        $summary_data = WeatherManager::getInstance()->getForecastFor($city);
        $meassure_points = count($summary_data);
        if ($meassure_points < 1) {
            throw new \Exception(sprintf(gettext("No weather data found for %s (%d)"), $city->getName(), $city->getID()));
        }
        $title = sprintf(
            gettext("Weather summary for %s at %s"),
            $city->getName(),
            date(gettext('Y-m-d H:i'), strtotime($summary_data[array_keys($summary_data)[0]]['dt']))
        );
        $padding_x = 30;
        $padding_y = 60;
        $bar_width = 40;
        $gap = $bar_width * 2;
        $width = ($padding_x * 2) + ($meassure_points * $bar_width) + (($meassure_points - 1) * $gap);
        $height = 600;
        $drawer = new Drawer($title, $width, $height);
        $drawer->addText(
            $drawer->getTitle(),
            $padding_x,
            $padding_y,
            $padding_y * 0.75,
            Drawer::COLOR_BLACK
        );
        $range = $height - (2 * $padding_y);
        $i = 0;
        $oldx = null;
        $oldy = null;
        foreach ($summary_data AS $timestamp => $data) {
            $temperature = floatval($data['temp']);
            if ($i > 0) {
                $oldx = $x;
                $oldy = $y;
            }
            $x = $padding_x + ($i * ($gap + $bar_width));
            $bar_height = ($range / 50) * $temperature;
            $y = $height - $padding_y - $bar_height;
            if ($i === 0) {
                $oldx = $x;
                $oldy = $y;
            }
            $color = self::temperatureToColor($temperature);
            $drawer->addLine(
                $x + ($bar_width / 2),
                $y,
                $oldx + ($bar_width / 2),
                $oldy
            );
            $drawer->addCircle(
                $x + ($bar_width / 2),
                $y,
                ($bar_width / 8),
                $color,
                $color
            );
            // Print the timestamp
            $drawer->addText(
                date('H:i', $timestamp),
                $x,
                $height - ($padding_y / 4),
                14
            );
            // Print the value
            $drawer->addText(
                $temperature .'°C',
                $x - 4,
                $y - 4,
                14,
                'gray'
            );
            $i++;
        }
        $filename = ROOT_DIRECTORY . DIRECTORY_SEPARATOR .'var'. DIRECTORY_SEPARATOR .'weathersummary_'. $city->getID() .'.png';
        file_put_contents($filename, $drawer->toPNG());
        return $filename;
    }

}
