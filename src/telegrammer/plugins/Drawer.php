<?php

namespace telegrammer\plugins;

use \Imagick;
use \ImagickPixel;
use \SimpleXMLElement;

/**
 * This class is meant to draw svg graphics.
 * @author Marius Timmer
 */
class Drawer {

    const COLOR_RED    = 'red';
    const COLOR_GREEN  = 'green';
    const COLOR_BLUE   = 'blue';
    const COLOR_YELLOW = 'yellow';
    const COLOR_BLACK  = 'black';
    const COLOR_WHITE  = 'white';

    /**
     * Title of this drawing which will also be the filename followed by the file extension.
     * @var string
     */
    private $title;

    /**
     * Width of this drawing.
     * @var int
     */
    private $width;

    /**
     * Height of this drawing.
     * @var int
     */
    private $height;

    /**
     * XML document which stores the graphic.
     * @var \SimpleXMLElement
     */
    private $document;

    public function __construct(string $title, int $width, int $height = 0) {
        $this->title = $title;
        $this->width = $width;
        $this->height = ($height > 0) ? $height : $width;
        $this->document = new SimpleXMLElement('<svg/>');
        $title_element = $this->document->addChild('title');
        $title_element[0] = $title;
        $this->document->addAttribute('width', $this->getWidth());
        $this->document->addAttribute('height', $this->getHeight());
        $this->document->addAttribute('xmlns', 'http://www.w3.org/2000/svg');
        $this->document->addAttribute('xmlns:xlink', 'http://www.w3.org/1999/xlink');
    }

    /**
     * Returns the title of this drawing.
     * @return string
     */
    public function getTitle(): string {
        return $this->title;
    }

    /**
     * Returns the filename of this drawing based on the title.
     * @return string
     */
    public function getFilename(): string {
        return $this->getTitle() .'.svg';
    }

    /**
     * Returns the filename of this drawing based on the title.
     * @return string
     */
    public function getPNGFilename(): string {
        return $this->getTitle() .'.png';
    }

    public function getWidth(): int {
        return $this->width;
    }

    public function getHeight(): int {
        return $this->height;
    }

    /**
     * Adds a rectangle to the document.
     * @param int $x X-Position
     * @param int $y Y-Position
     * @param int $width Width
     * @param int $height Height
     * @param string $border_color Color of the border / stroke
     * @param string $fill_color Fill color or empty for transparent
     * @param string $title Title / Tooltip
     */
    public function addRectangle(int $x, int $y, int $width, int $height, string $border_color = self::COLOR_BLACK, string $fill_color = 'none', string $title = '') {
        $element = $this->document->addChild('rect');
        $element->addAttribute('x', $x);
        $element->addAttribute('y', $y);
        $element->addAttribute('width', $width);
        $element->addAttribute('height', $height);
        $element->addAttribute('stroke', $border_color);
        if (!empty($title)) {
            $element->addAttribute('title', $title);
        }
        if (!empty($fill_color)) {
            $element->addAttribute('fill', $fill_color);
        }
    }

    public function addLine(int $x1, int $y1, int $x2, int $y2, $color = self::COLOR_BLACK, int $thickness = 1) {
        $element = $this->document->addChild('line');
        $element->addAttribute('x1', $x1);
        $element->addAttribute('y1', $y1);
        $element->addAttribute('x2', $x2);
        $element->addAttribute('y2', $y2);
        $element->addAttribute('stroke-width', $thickness);
        if (!empty(trim($color))) {
            $element->addAttribute('stroke', $color);
        }
    }

    public function addCircle(int $x, int $y, int $radius, $color = self::COLOR_BLACK, string $fill = self::COLOR_BLACK) {
        $element = $this->document->addChild('circle');
        $element->addAttribute('cx', $x);
        $element->addAttribute('cy', $y);
        $element->addAttribute('r', $radius);
        if (!empty(trim($color))) {
            $element->addAttribute('stroke', $color);
        }
        if (!empty(trim($fill))) {
            $element->addAttribute('fill', $fill);
        }
    }

    public function addText(string $text, int $x, int $y, int $size = 12, string $color = self::COLOR_BLACK, int $rotate = 0) {
        $element = $this->document->addChild('text');
        $element->addAttribute('x', $x);
        $element->addAttribute('y', $y);
        $element->addAttribute(
            'style',
            sprintf(
                'font-size: %dpx; fill: %s;',
                $size,
                $color
            )
        );
        if ($rotate !== 0) {
            $cx = (strlen($text) * $size) / 2;
            $cy = $size / 2;
            $element->addAttribute(
                'transform',
                sprintf(
                    'rotate(%d,%d,%d)',
                    $rotate,
                    $x + $cx,
                    $y + $cy
                )
            );
        }
        $element[0] = $text;
    }

    public static function getRandomColor(): string {
        $r = rand(128, 255);
        $g = rand(128, 255);
        $b = rand(128, 255);
        return sprintf('rgb(%d,%d,%d)', $r, $g, $b);
    }

    public function __toString(): string {
        $dom = dom_import_simplexml($this->document)->ownerDocument;
        $dom->formatOutput = true;
        return $dom->saveXML();
    }

    /**
     * Raw Imace blob.
     * @return string
     */
    public function toPNG() {
        $im = new Imagick();
        $im->readimageblob(strval($this));
        $im->setimagebackgroundcolor(new ImagickPixel('transparent'));
        $im->setimageformat('png24');
        $im->resizeimage($this->getWidth(), $this->getHeight(), Imagick::FILTER_LANCZOS, 1);
        $blob = $im->getimageblob();
        $im->clear();
        $im->destroy();
        return $blob;
    }

}
