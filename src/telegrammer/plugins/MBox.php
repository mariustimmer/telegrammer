<?php

namespace telegrammer\plugins;


class MBox {

    const DIRECTORY_MBOX = '/var/spool/mail';
    const MAIL_SPLITTER = PHP_EOL . PHP_EOL .'From ';

    /**
     *
     * @var MBox
     */
    private static $instance;

    /**
     * Name of the user we use the mailbox of.
     * @var string
     */
    private $username;

    /**
     * Parsed mails.
     * @var EMail[]
     */
    private $mails;

    /**
     * returns the mbox file for the requested user.
     * @param string $username
     * @return string
     */
    public static function getMBoxFile(string $username): string {
        return self::DIRECTORY_MBOX . DIRECTORY_SEPARATOR . $username;
    }

    public static function getInstance(string $username = null): MBox {
        if (!self::$instance) {
            if (empty($username)) {
                $username = get_current_user();
            }
            self::$instance = new MBox($username);
        }
        return self::$instance;
    }

    private function parseMBoxFile() {
        $this->mails = [];
        $filename = self::getMBoxFile($this->getUsername());
        $raw_content = file_get_contents($filename);
        if ($raw_content === false) {
            return false;
        }
        $raw_mails = explode(self::MAIL_SPLITTER, $raw_content);
        foreach ($raw_mails AS $mail) {
            $mail = 'From '. $mail;
            if (strpos($mail, PHP_EOL . PHP_EOL) === false) {
                if (count($this->mails) > 0) {
                    $this->mails[count($this->mails) - 1]['body'] .= $mail;
                }
                continue;
            }
            list($header, $body) = explode(PHP_EOL . PHP_EOL, $mail, 2);
            $header_fields = [];
            foreach (explode(PHP_EOL, $header) AS $line) {
                if (strpos($line, ': ') === false) {
                    continue;
                }
                list($key, $value) = explode(': ', $line);
                $header_fields[strtolower($key)] = $value;
            }
            if ((!isset($header_fields['from'])) ||
                (!isset($header_fields['subject']))) {
                if (count($this->mails) > 0) {
                    $this->mails[count($this->mails) - 1]['body'] .= $mail;
                }
                continue;
            }
            array_push(
                $this->mails,
                new EMail($header_fields, $body)
            );
        }
    }

    /**
     * 
     * @return EMail[]
     */
    public function getMails(): array {
        return $this->mails;
    }

    /**
     * Searches for the requested mail.
     * @param string $message_id
     * @return EMail
     */
    public function getMail(string $message_id) {
        foreach ($this->getMails() AS $mail) {
            /* @var $mail EMail */
            if ($mail->getMessageID() == $message_id) {
                return $mail;
            }
        }
        return null;
    }

    public function countMails(): int {
        return count($this->getMails());
    }

    private function __construct(string $username) {
        $this->username = $username;
        $this->parseMBoxFile();
    }

    public function getUsername(): string {
        return $this->username;
    }

}
