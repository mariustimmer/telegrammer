<?php

namespace telegrammer\plugins;

use \Exception AS Exception;
use \SimpleXMLElement AS SimpleXMLElement;
use \telegrammer\TelegramAPI AS TelegramAPI;

class FefeFetcher {

    const BRAINFILE = ROOT_DIRECTORY . DIRECTORY_SEPARATOR . 'fefe.json';
    const URL = 'https://blog.fefe.de/rss.xml';

    /**
     *
     * @var array
     */
    private $brain;

    private $messages;

    public function __construct() {
        $this->loadBrain();
    }

    private function loadBrain() {
        $this->brain = (array) json_decode(file_get_contents(self::BRAINFILE));
        if (!$this->brain) {
            $this->brain = [];
        }
    }

    public function fetch() {
        $rss = file_get_contents(self::URL);
        if (empty(trim($rss))) {
            throw new Exception(
                gettext("Could not download RSS")
            );
        }
        $this->parse($rss);
        $admin_chat_id = TelegramAPI::getInstance()->getAdmin();
        foreach ($this->messages AS $message) {
            TelegramAPI::getInstance()->send($admin_chat_id, $message, TelegramAPI::PARSEMODE_MARKDOWN);
        }
        file_put_contents(
            self::BRAINFILE,
            json_encode(
                $this->getBrain(),
                JSON_PRETTY_PRINT
            )
        );
    }

    public static function getCutLength(): int {
        return 128;
    }

    public function getBrain(): array {
        return $this->brain;
    }

    /**
     * Adds a new link to the brain so it wont be used again.
     * @param string $url
     */
    public function addToBrain(string $url) {
        $this->brain[$url] = time();
    }

    /**
     * Parses articles from the downloaded content.
     * @param string $rss Downloaded content
     */
    private function parse(string $rss) {
        $this->messages = [];
        $xml = new SimpleXMLElement($rss);
        if (!$xml) {
            throw new Exception(
                gettext("Could not parse XML")
            );
        }
        foreach ($xml->channel->item AS $item) {
            $link = strval($item->link[0]);
            $text = strval($item->title[0]);
            if (strlen($text) > self::getCutLength()) {
                $text = substr(
                    $text,
                    0,
                    strpos(
                        $text,
                        ' ',
                        self::getCutLength()
                    )
                ) .' ...';
            }
            if (array_key_exists($link, $this->getBrain())) {
                continue;
            }
            $message = sprintf(
                gettext("*Fefe*: %s [Link](%s)"),
                $text,
                $link
            );
            array_push($this->messages, $message);
            $this->addToBrain($link);
        }
    }

}
