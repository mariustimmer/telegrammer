<?php

namespace telegrammer;

use \CURLFile AS CURLFile;
use \Exception AS Exception;
use \telegrammer\Configuration AS Configuration;
use \telegrammer\database\ChatManager AS ChatManager;
use \telegrammer\Espeak AS Espeak;
use \telegrammer\telegram\File AS File;
use \telegrammer\telegram\Message AS Message;
use \telegrammer\telegram\Update AS Update;
use \telegrammer\telegram\User AS User;

class TelegramAPI {

    const KEY_OK          = 'ok';
    const KEY_DESCRIPTION = 'description';
    const KEY_RESULT      = 'result';
    const ADMIN_NAME         = 0;
    const ADMIN_CHATID       = 1;
    const URL_TEMPLATE       = 'https://api.telegram.org/bot%s/%s';
    const PARSEMODE_TEXT     = '';
    const PARSEMODE_MARKDOWN = 'Markdown';
    const WEBHOOK_MAX_CONNECTIONS_DEFAULT = 20;

    /**
     * Singelton object
     * @var TelegramAPI
     */
    private static $instance;

    /**
     * Detects "syntax errors" in markdown text by checking if any special
     * characters are missing. If so they all will be removed.
     * @param string $text Text to check
     * @return string
     */
    public static function secureMarkdown(string $text): string {
        $special_chars = ['_', '*'];
        foreach ($special_chars AS $special_char) {
            if (substr_count($text, $special_char) % 2 != 0) {
                $text = str_replace($special_char, '', $text);
            }
        }
        return $text;
    }

    /**
     * Sends a message to a chat and stores the sent message in our database
     * on success.
     * @param int $chat_id Chat-ID
     * @param string $message Message
     * @param string $parse_mode Mode of the message (PARSEMODE_TEXT or PARSEMODE_MARKDOWN)
     * @return bool
     */
    public function send(int $chat_id, string $message, string $parse_mode = self::PARSEMODE_TEXT): bool {
        if ($parse_mode === self::PARSEMODE_MARKDOWN) {
            /**
             * If using Markdown make sure there are no missing special chars
             * like * or _. They will be removed if "syntax error" is detected.
             */
            $message = self::secureMarkdown($message);
        }
        $response = $this->communicate(
            'sendMessage',
            [
                'chat_id'    => $chat_id,
                'text'       => $message,
                'parse_mode' => $parse_mode
            ]
        );
        $message_obj = new Message($response[self::KEY_RESULT]);
        ChatManager::getInstance()->upsertMessage($message_obj);
        return true;
    }

    /**
     * Sets the webhook of the bot to match the given parameters.
     * @param string $url URL of the webhook
     * @param string $certificate Path to the certificate file
     * @return bool
     */
    public function setWebhook(string $url, string $certificate_path = ''): bool {
        $data = [
            'url'             => $url,
            'max_connections' => self::WEBHOOK_MAX_CONNECTIONS_DEFAULT
        ];
        if (!empty(trim($certificate_path))) {
            $data['certificate'] = new Curlfile(
                realpath($certificate_path),
                'application/x-pem-file',
                basename($certificate_path)
            );
        }
        $response = $this->communicate('setWebhook', $data);
        return (($response[self::KEY_OK]) && ($response[self::KEY_RESULT]));
    }

    /**
     * Sends a photo to a chat.
     * @param int $chat_id
     * @param string $photopath
     * @param string $caption
     */
    public function sendPhoto(int $chat_id, string $photopath, string $caption) {
        $curlfile = new Curlfile(realpath($photopath), 'image/svg+xml', basename($photopath));
        $response = $this->communicate(
            'sendPhoto',
            [
                'chat_id'  => $chat_id,
                'caption'  => $caption,
                'photo'    => $curlfile
            ]
        );
        $message_obj = new Message($response[self::KEY_RESULT]);
        ChatManager::getInstance()->upsertMessage($message_obj);
        return true;
    }

    /**
     * Generates a voice message according to the text and sends it to the
     * requested chat. On success it will be stored in our database.
     * @param int $chat_id
     * @param string $message
     * @param string $caption
     * @param string $voice Voice to use (Espeak::VOICE_*-Constant)
     * @return bool
     */
    public function sendVoice(int $chat_id, string $message, string $caption = null, string $voice = Espeak::VOICE_ENGLISH_MALE) {
        Espeak::getInstance()->setVoice($voice);
        $curlfile = new CURLFile(Espeak::getInstance()->textToSpeech($message));
        $response = $this->communicate(
            'sendVoice',
            [
                'chat_id'    => $chat_id,
                'voice'      => $curlfile,
                'caption'    => $caption
            ]
        );
        $message_obj = new Message($response[self::KEY_RESULT]);
        ChatManager::getInstance()->upsertMessage($message_obj);
        return true;
    }

    /**
     * Requests the file path to the given file id and returns the file object.
     * @param string $file_id
     * @return File
     */
    public function getFile(string $file_id): File {
        $response = $this->communicate(
            'getFile',
            [
                'file_id' => $file_id
            ]
        );
        if (empty($response[self::KEY_RESULT])) {
            return null;
        }
        return new File($response[self::KEY_RESULT]);
    }

    /**
     * Downloads the requested file from the given path which can be found
     * by the given file.
     * @param File $file
     * @throws Exception
     */
    public function downloadFile(File $file) {
        $url = sprintf(
            'https://api.telegram.org/file/bot%s/%s',
            Configuration::getInstance()->get('Telegram', 'APIKEY'),
            $file->getFilePath()
        );
        $response = file_get_contents($url);
        if ($response === false) {
            throw new Exception(gettext("Could not download file!"));
        }
        $filename = ROOT_DIRECTORY . DIRECTORY_SEPARATOR . basename($file->getFilePath());
        file_put_contents($filename, $response);
    }

    /**
     * Requests all available updates from the API which arrived within the
     * past 24 hours.
     * @return Update[]
     */
    public function getUpdates(): array {
        $response = $this->communicate(
            'getUpdates'
        );
        $updates = [];
        foreach ($response[self::KEY_RESULT] AS $json_update) {
            array_push($updates, new Update($json_update));
        }
        return $updates;
    }

    /**
     * Finds out who this bot is and return its user object.
     * @return User
     */
    public function getMe() {
        $response = $this->communicate('getMe');
        return new User($response[self::KEY_RESULT]);
    }

    private function communicate(string $method, array $parameter = []): array {
        $url = sprintf(
            self::URL_TEMPLATE,
            Configuration::getInstance()->get('Telegram', 'APIKEY'),
            $method
        );
        $curl_handler = curl_init();
        curl_setopt_array(
            $curl_handler,
            [
                CURLOPT_URL            => $url,
                CURLOPT_RETURNTRANSFER => 1,
                CURLOPT_POST           => true,
                CURLOPT_POSTFIELDS     => $parameter,
                CURLOPT_HTTPHEADER     => ["Content-Type: multipart/form-data"]
            ]
        );
        $response = curl_exec($curl_handler);
        if ($response === false) {
            throw new Exception(curl_error($curl_handler));
        }
        $json = json_decode($response, true);
        if ((!is_array($json)) ||
            (empty($json[self::KEY_OK])) ||
            (!$json[self::KEY_OK])) {
            throw new Exception($json[self::KEY_DESCRIPTION] .': '. $method.'/'. json_encode($parameter), $json['error_code']);
        }
        return $json;
    }

    /**
     * Reads out information about the administrator.
     * @param int $info ADMIN_CHATID or ADMIN_NAME
     * @return string
     */
    public function getAdmin(int $info = self::ADMIN_CHATID) {
        $admin = explode(':', Configuration::getInstance()->get('Telegram', 'ADMINISTRATOR'));
        return $admin[$info];
    }

    public static function getInstance(): TelegramAPI {
        if (!self::$instance) {
            self::$instance = new TelegramAPI();
        }
        return self::$instance;
    }

}
