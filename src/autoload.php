<?php

define('ROOT_DIRECTORY', __DIR__ . DIRECTORY_SEPARATOR .'..');

/**
 * Basic autoloader to load required classes the most simple way by
 * converting the namespace to a path and append it to the current directory.
 * @param string $classname Namespace of the class
 * @return bool True on success or false
 */
$base_autoloader = function(string $classname): bool {
	$relative_path = str_replace("\\", DIRECTORY_SEPARATOR, $classname) .'.php';
	$classfile = __DIR__ . DIRECTORY_SEPARATOR . $relative_path;
	if (file_exists($classfile)) {
		require_once($classfile);
		return true;
	}
	return false;
};
spl_autoload_register($base_autoloader);
