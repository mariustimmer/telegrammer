#!/usr/bin/env php
<?php

require_once(__DIR__ .'/../src/autoload.php');
chdir(ROOT_DIRECTORY);

use \telegrammer\application\Daemon AS Daemon;

$daemon = new Daemon();
