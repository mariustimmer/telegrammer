#!/usr/bin/env php
<?php

require_once(__DIR__ .'/../src/autoload.php');
chdir(ROOT_DIRECTORY);

use \Exception AS Exception;
use \telegrammer\Configuration AS Configuration;
use \telegrammer\TelegramAPI AS TelegramAPI;

if (count($argv) !== 3) {
    fprintf(
        STDERR,
        "%s!\n",
        gettext("2 arguments (chat id and message) required")
    );
    exit(1);
}

$chat_id = $argv[1];
if ($chat_id === 'admin' || $chat_id === 'administrator') {
    $chat_id = TelegramAPI::getInstance()->getAdmin(TelegramAPI::ADMIN_CHATID);
}
$message = $argv[2];

try {
    if (TelegramAPI::getInstance()->send($chat_id, $message, TelegramAPI::PARSEMODE_MARKDOWN)) {
        fprintf(
            STDOUT,
            "%s\n",
            gettext("Okay")
        );
    } else {
        throw new Exception(gettext("Unknown error"));
    }
} catch (Exception $exception) {
    fprintf(
        STDERR,
        "Fatal error: %s\n",
        $exception->getMessage()
    );
    exit(1);
}
