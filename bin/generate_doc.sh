#!/bin/bash

PHPDOC=/usr/bin/phpdoc
DIR_SCRIPT="$(cd "$(dirname "$0")" && pwd)"
DIR_ROOT="${DIR_SCRIPT}/.."
DIR_BUILD="${DIR_ROOT}/build"
DIR_TARGET="${DIR_ROOT}/doc/api"
DIR_SOURCE="${DIR_ROOT}/src"
TITLE="Telegrammer"

${PHPDOC} project:run \
	--target="${DIR_TARGET}" \
	--directory="${DIR_SOURCE}" \
	--title="${TITLE}" \
	--visibility="public,protected"
rm -rf "${DIR_BUILD}"

