#!/usr/bin/env php
<?php

/**
 * Sets the Webhook
 * Contacts the Telegram API and sets the webhook according
 * to the current configuration.
 * @author Marius Timmer
 * @see https://core.telegram.org/bots/api#setwebhook
 */

require_once(__DIR__ .'/../src/autoload.php');
chdir(ROOT_DIRECTORY);

use \Exception AS Exception;
use \telegrammer\Configuration AS Configuration;
use \telegrammer\TelegramAPI AS TelegramAPI;

$url = Configuration::getInstance()->get('Webhook', 'URL');
$certificate = Configuration::getInstance()->get('Webhook', 'CERTIFICATE');

if (empty(trim($url))) {
    fprintf(
        STDERR,
        "'%s' in configuration is required!\n",
        'Webhook::URL'
    );
    exit(1);
}

if (!empty(trim($certificate))) {
    if (!file_exists($certificate)) {
        fprintf(
            STDERR,
            "Certificate file \"%s\" does not exists!\n",
            $certificate
        );
        exit(1);
    }
    
    if (!is_readable($certificate)) {
        fprintf(
            STDERR,
            "Certificate file \"%s\" is not readable!\n",
            $certificate
        );
        exit(1);
    }
}

try {
    if (TelegramAPI::getInstance()->setWebhook($url, $certificate)) {
        fprintf(
            STDOUT,
            "%s\n",
            gettext("Okay")
        );
    } else {
        throw new Exception(gettext("Unknown error"));
    }
} catch (Exception $exception) {
    fprintf(
        STDERR,
        "Fatal error: %s\n",
        $exception->getMessage()
    );
    exit(1);
}
