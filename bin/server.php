<?php

if ((!isset($_SERVER['REQUEST_METHOD'])) ||
    ($_SERVER['REQUEST_METHOD'] !== 'POST')) {
    /**
     * This script should not be executed within the command line and must not
     * be called by a browser. To make that a bit more sure we tell our client
     * to use the POST method since Telegramm will use it.
     */
    header("Allow: POST");
    http_response_code(405);
    die();
}

/**
 * Here we can be sure that this is a POST request and hope that the data
 * contains a valid update. But to make that sure we include our requirements.
 */
require_once(__DIR__ .'/../src/autoload.php');
chdir(ROOT_DIRECTORY);

use \Exception AS Exception;
use \telegrammer\database\ChatManager;
use \telegrammer\Flynn;
use \telegrammer\telegram\Update;

/**
 * Lets read out and parse the input data. If the data is empty or not a
 * valid JSON string we abort without saying anything.
 */
$data = file_get_contents('php://input');
if ($data === false) {
    http_response_code(400);
    die();
}
$json = json_decode($data, true);
if ($json === false) {
    http_response_code(400);
    die();
}

/**
 * Finally the input data can be converted to
 * an update and it is time to process it.
 */
try {
    $update = new Update($json);
    if (ChatManager::getInstance()->getUpdateByID($update->getID()) === null) {
        /**
         * We noticed that the updates may be sent multiple times to this
         * server. To prevent ourselfes from sending too many responses we
         * will check weather this update already has been processed.
         */
        Flynn::getInstance()->processMessage($update->getMessage());
        ChatManager::getInstance()->upsertUpdate($update);
    }
    http_response_code(200);
    die();
} catch(Exception $exception) {
    trigger_error($exception->getFile() .':'. $exception->getLine() .': '. $exception->getMessage());
    http_response_code(500);
    die();
}
