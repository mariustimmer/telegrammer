#!/usr/bin/env php
<?php

/**
 * This cron script will be called once a hour to update the weather
 * forecasts for the administrative users.
 */

require_once(__DIR__ .'/../src/autoload.php');
chdir(ROOT_DIRECTORY);

use \Exception AS Exception;
use \telegrammer\database\WeatherManager AS WeatherManager;
use \telegrammer\openweathermap\API AS API;

try {
    WeatherManager::getInstance()->deprovisioning();
    $cities = [];
    $observations = WeatherManager::getInstance()->getObservators();
    foreach ($observations AS $observation) {
        $key = $observation['name'] .','. $observation['country'];
        if (!isset($cities[$key])) {
            $cities[$key] = [];
        }
        $cities[$key][$observation['userid']] = $observation['username'];
    }
    foreach ($cities AS $location_name => $userdata) {
        API::getInstance()->updateForecasts($location_name);
    }
} catch (Exception $exception) {
    fprintf(
        STDERR,
        "Fatal error: %s\n",
        $exception->getMessage()
    );
    exit(1);
}
