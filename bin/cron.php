#!/usr/bin/env php
<?php

/**
 * This script will be called each 5 minutes to handle incomming messages.
 */

require_once(__DIR__ .'/../src/autoload.php');
chdir(ROOT_DIRECTORY);

use \telegrammer\Flynn AS Flynn;
use \telegrammer\database\ChatManager;
use \telegrammer\telegram\Update;
use \telegrammer\TelegramAPI;
use \telegrammer\plugins\FefeFetcher;

try {
    /**
     * First of all the news will be fetched and the admin will be
     * informed about it if there is something new.
     */
    $fefefetcher = new FefeFetcher();
    $fefefetcher->fetch();
    
    /* @var $updates Update[] */
    //// Fetching updates not possible - webhook enabled
    //$updates = TelegramAPI::getInstance()->getUpdates();
    //foreach ($updates AS $update) {
    //    if (ChatManager::getInstance()->getUpdateByID($update->getID()) === null) {
    //        fprintf(
    //            STDOUT,
    //            gettext("Found new Update %d") . PHP_EOL,
    //            $update->getID()
    //        );
    //        Flynn::getInstance()->processMessage($update->getMessage());
    //        ChatManager::getInstance()->upsertUpdate($update);
    //    }
    //}
} catch (Exception $exception) {
    fprintf(
        STDERR,
        "Fatal error: %s\n",
        $exception->getMessage()
    );
    exit(1);
}
