#!/bin/bash

# Scan for gettext usages
# This script searches the php sources fpr occurences of gettext and adds
# them to the pot and po files. After running this script you have to add
# the new translations in the po file. After that you can execute this
# script again to generate the required mo file which will be finally used
# by the php sources.
# Added 2020-09-18

DOMAIN=telegrammer
ROOT_DIRECTORY=$( cd "$(dirname "${BASH_SOURCE[0]}")" ; pwd -P )/..
LOCALE_DIRECTORY="${ROOT_DIRECTORY}/var/locale"
SOURCES_DIRECTORY="${ROOT_DIRECTORY}/src"

mkdir -p "${LOCALE_DIRECTORY}/de_DE/LC_MESSAGES"

xgettext \
	--directory=./ \
	--default-domain="${DOMAIN}" \
	--output="${LOCALE_DIRECTORY}/de_DE/LC_MESSAGES/${DOMAIN}.pot" \
	--language=PHP \
	--from-code=UTF-8 \
	--indent \
	--no-location \
	--no-wrap \
	--sort-output \
	--package-name="${DOMAIN}" \
	--package-version=1.0 \
	--msgid-bugs-address="Marius Timmer <admin@mariustimmer.de>" \
	$(find "${SOURCES_DIRECTORY}" -type f -name "*.php")

msgmerge \
  --update \
	--indent \
	--lang=German \
	--no-location \
	--no-wrap \
	--sort-output \
	--no-fuzzy-matching \
	"${LOCALE_DIRECTORY}/de_DE/LC_MESSAGES/${DOMAIN}.po" \
	"${LOCALE_DIRECTORY}/de_DE/LC_MESSAGES/${DOMAIN}.pot"

msgfmt \
	--output="${LOCALE_DIRECTORY}/de_DE/LC_MESSAGES/${DOMAIN}.mo" \
	"${LOCALE_DIRECTORY}/de_DE/LC_MESSAGES/${DOMAIN}.po"