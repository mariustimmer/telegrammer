Telegrammer
===========

This application started as simple Telegram bot interface but was extended to support the interfaces of OpenWeatherMap, UNIX mboxes and more. It is supposed to assist its users and specially its administrators.

Since this is a Telegram bot it supports the update pull and the adhoc update method. The full list of available commands, must not be published.

Requirements
------------
This software consists of two parts: The executable scripts to communicate with the Telegram-API and the server side which can be used to use webhooks (_configure the webbhook and run [setWebhook.php](./bin/setWebhook.php)_).

 - Apache web server with php enabled
 - php-sqlite driver module
 - php-curl module
 - espeak in order to generate voice messages